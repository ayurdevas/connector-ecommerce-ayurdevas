# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import logging
import requests

from odoo.addons.component.core import Component
from odoo.addons.queue_job.exception import RetryableJobError
from odoo.addons.connector.exception import IDMissingInBackend
from datetime import datetime
from contextlib import contextmanager

_logger = logging.getLogger(__name__)

try:
    import ayurdevas as ayurdevaslib
except ImportError:
    _logger.debug("Cannot import 'ayurdevas'")

AYURDEVAS_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


class AyurdevasAPI(object):

    def __init__(self, location, token):
        self._location = location
        self._token = token
        self._api = None

    @property
    def api(self):
        if self._api is None:
            api = ayurdevaslib.Api(self._location, self._token)
            self._api = api
        return self._api

    def __enter__(self):
        """ Entry point for with statement """
        return self

    def __exit__(self, type, value, traceback):
        """ Exit point """
        return

    def call(self, method, *args):
        start = datetime.now()
        try:
            result = getattr(self.api, method)(*args)
        except requests.exceptions.ConnectionError as err:
            raise RetryableJobError(
                    'A network error caused the failure of the job:\n'
                    'Error message: %s\n' % (err)
                )
        except requests.exceptions.HTTPError as err:
            if err.response.status_code in [502,   # Bad gateway
                                            503,   # Service unavailable
                                            504]:  # Gateway timeout
                raise RetryableJobError(
                    'A protocol error caused the failure of the job:\n%s' % err
                )
            else:
                raise
        except:
            _logger.error("api.%s(%s) failed", method, args)
            raise
        else:
            _logger.debug("api.%s(%s) returned %s in %s seconds",
                          method, args, result,
                          (datetime.now() - start).seconds)
        return result


class AyurdevasAdapter(Component):
    """ Generic adapter for using the Ayurdevas backend """
    _name = 'ayurdevas.adapter'
    _inherit = ['base.backend.adapter.crud', 'ayurdevas.base']
    _usage = 'backend.adapter'

    def _call(self, callback, *args):
        try:
            ayurdevas_api = getattr(self.work, 'ayurdevas_api')
        except AttributeError:
            raise AttributeError(
                'You must provide a ayurdevas_api attribute with a '
                'Ayurdevas.Api instance to be able to use the '
                'Backend Adapter.'
            )
        return ayurdevas_api.call(callback, *args)

    @contextmanager
    def handle_404(self):
        """Context manager to handle 404 errors on the API

        404 (no record found) on the API are re-raised as:
        ``odoo.addons.connector.exception.IDMissingInBackend``
        """
        try:
            yield
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 404:
                raise IDMissingInBackend(str(err))
            raise
