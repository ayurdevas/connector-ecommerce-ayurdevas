# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from datetime import datetime

from odoo import fields
from odoo.addons.component.core import AbstractComponent

from .backend_adapter import AYURDEVAS_DATETIME_FORMAT

"""

Exporters for Ayurdevas Ecommerce.

In addition to its export job, an exporter has to:

* check in Ayurdevas ecommerce if the record has been updated more
  recently than the last sync date and if yes, delay an import
* call the ``bind`` method of the binder to update the last sync date

"""


class AyurdevasBaseExporter(AbstractComponent):
    """ Base exporter for Ayurdevas """

    _name = 'ayurdevas.base.exporter'
    _inherit = ['generic.exporter', 'ayurdevas.base']
    _usage = 'record.exporter'
    _default_binding_field = 'ayurdevas_bind_ids'

    def _should_import(self):
        """ Before the export, compare the update date
        in Ayurdevas and the last sync date in Odoo,
        if the former is more recent, schedule an import
        to not miss changes done in Ayurdevas.
        """
        assert self.binding
        if not self.external_id:
            return False
        sync = self.binder.sync_date(self.binding)
        if not sync:
            return True
        record = self.backend_adapter.read(
            self.external_id,
            fields=['updated_at']
        )
        if not record['updated_at']:
            return True
        sync_date = fields.Datetime.from_string(sync)
        external_date = datetime.strptime(
            record['updated_at'],
            AYURDEVAS_DATETIME_FORMAT
        )
        return sync_date < external_date

    def _lock(self):
        """ Lock the binding record.

        Lock the binding record so we are sure that only one export
        job is running for this record if concurrent jobs have to export the
        same record.

        When concurrent jobs try to export the same record, the first one
        will lock and proceed, the others will fail to lock and will be
        retried later.

        This behavior works also when the export becomes multilevel
        with :meth:`_export_dependencies`. Each level will set its own lock
        on the binding record it has to export.
        """
        self.component('record.locker').lock(self.binding)
