# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import re

from odoo.addons.component.core import AbstractComponent
import collections


class AyurdevasExportMapper(AbstractComponent):
    _name = 'ayurdevas.export.mapper'
    _inherit = ['ayurdevas.base', 'base.export.mapper']
    _usage = 'export.mapper'


class AyurdevasImportMapper(AbstractComponent):
    _name = 'ayurdevas.import.mapper'
    _inherit = ['ayurdevas.base', 'base.import.mapper']
    _usage = 'import.mapper'


def normalize_datetime(field):
    """Change a invalid date which comes from Ayurdevas Ecommerce"""

    def modifier(self, record, to_attr):
        if record[field] == '0000-00-00 00:00:00':
            return None
        return record[field]
    return modifier


def normalize_date(field):
    """Change a invalid date which comes from Ayurdevas Ecommerce"""

    def modifier(self, record, to_attr):
        if record[field] == '0000-00-00' or not record[field]:
            return None
        return record[field]
    return modifier


def titlecase(field):
    """Return a titlecased version of the string where words start with an
    uppercase character and the remaining characters are lowercase.
    It can be used in a pipeline of modifiers.
    """

    def modifier(self, record, to_attr):
        if isinstance(field, collections.Callable):
            result = field(self, record, to_attr)
        else:
            result = record[field]
        return result.title()
    return modifier


def normalize_name(field):
    """ Return a lowercased striped version of the string
    It can be used in a pipeline of modifiers.
    """

    def modifier(self, record, to_attr):
        if isinstance(field, collections.Callable):
            result = field(self, record, to_attr)
        else:
            result = record[field]
        return result.lower().replace('<<baja>>', '').strip()
    return modifier
