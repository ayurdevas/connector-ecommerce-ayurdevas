# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import fields
from odoo.addons.component.core import Component


class AyurdevasModelBinder(Component):
    """ Bind records and give odoo/ayurdevas ids correspondence

    Binding models are models called ``ayurdevas.{normal_model}``,
    like ``ayurdevas.res.partner`` or ``ayurdevas.product.product``.
    They are ``_inherits`` of the normal models and contains
    the Ayurdevas ID, the ID of the Ayurdevas Backend and the additional
    fields belonging to the ayurdevas instance.
    """
    _name = 'ayurdevas.binder'
    _inherit = ['base.binder', 'ayurdevas.base']
    _apply_on = [
        'ayurdevas.res.partner',
        'ayurdevas.sale.order',
        'ayurdevas.product.product',
        'ayurdevas.points.line',
        'ayurdevas.sales.campaign',
        'ayurdevas.miles.line',
        'ayurdevas.address',
    ]

    def sync_date(self, binding):
        assert self._sync_date_field
        sync_date = binding[self._sync_date_field]
        if not sync_date:
            return
        return fields.Datetime.from_string(sync_date)
