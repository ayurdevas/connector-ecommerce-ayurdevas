# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

"""

Importers for Ayurdevas Ecommerce.

An import can be skipped if the last sync date is more recent than
the last update in Ayurdevas Ecommerce.

They should call the ``bind`` method if the binder even if the records
are already bound, to update the last sync date.

"""

import logging
from contextlib import contextmanager
from psycopg2 import IntegrityError, errorcodes

from odoo import fields, _
from odoo.addons.component.core import AbstractComponent
from odoo.addons.queue_job.exception import RetryableJobError
from odoo.addons.connector.exception import IDMissingInBackend

_logger = logging.getLogger(__name__)


class AyurdevasBaseImporter(AbstractComponent):
    """ Base importer for Ayurdevas Ecommerce """

    _name = 'ayurdevas.base.importer'
    _inherit = ['base.importer', 'ayurdevas.base']
    _usage = 'record.importer'

    def _get_external_data(self):
        """ Return the raw data for ``self.external_id`` """
        return self.backend_adapter.read(self.external_id)

    def _must_skip(self, force=False):
        """ Hook called right after we read the data from the backend.

        If the method returns a message giving a reason for the
        skipping, the import will be interrupted and the message
        recorded in the job (if the import is called directly by the
        job, not by dependencies).

        If it returns None, the import will continue normally.

        :returns: None | str | unicode
        """
        assert self.external_record

    def _get_binding(self):
        """ Return the binding id from the backend id """
        return self.binder.to_internal(self.external_id)

    def _is_uptodate(self, binding):
        """ Return True if the import should be skipped because
        it is already up-to-date in Odoo """
        external_date = self.external_record.get('updated_at')
        if not external_date:
            return  False # no update date on Ecommerce, always import it.
        if not binding:
            return  False # it does not exist so it should not be skipped
        # We store the backend "updated_at" field in the binding,
        # so for further imports, we can check accurately if the
        # record is already up-to-date
        sync_date = binding.sync_date
        if not sync_date:
            return False # it's never been imported so it should not be skipped
        sync_date = fields.Datetime.from_string(binding.sync_date)
        external_date = fields.Datetime.from_string(external_date)
        return external_date < sync_date

    def _before_import(self):
        """ Hook called before the import, when we have the backend data """

    def _import_dependency(self, external_id, binding_model,
                           component=None, always=False):
        """
        Import a dependency.

        The component that will be used for the dependency can be injected
        with the ``component``.

        :param external_id: id of the related binding to import
        :param binding_model: name of the binding model for the relation
        :type binding_model: str | unicode
        :param component: component to use for the importer
                          By default: lookup component for the model with
                          usage ``record.importer``
        :param always: if True, the record is updated even if it already
                       exists, note that it is still skipped if it has
                       not been modified on backend since the last
                       update. When False, it will import it only when
                       it does not yet exist.
        :type always: boolean
        """
        if not external_id:
            return
        binder = self.binder_for(binding_model)
        if always or not binder.to_internal(external_id):
            if component is None:
                component = self.component(
                    usage='record.importer',
                    model_name=binding_model
                )
            component.run(external_id, force=True)

    def _import_dependencies(self):
        """ Import the dependencies for the record """
        return

    def _map_data(self):
        """ Returns an instance of
        :py:class:`~odoo.addons.component.core.Component`

        """
        return self.mapper.map_record(self.external_record)

    def _update_data(self, map_record, **kwargs):
        """ Get the data to pass to :py:meth:`_update` """
        return map_record.values(**kwargs)

    def _validate_data(self, data):
        """ Check if the values to import are correct

        Pro-actively check before the ``_create`` or
        ``_update`` if some fields are missing or invalid.

        Raise `InvalidDataError`
        """
        return

    def _update(self, binding, data):
        """ Update an Odoo record """
        # special check on data before import
        self._validate_data(data)
        binding_ctx = binding.with_context(connector_no_export=True)
        binding_ctx.sudo().write(data)
        _logger.debug('%d updated from backend %s', binding, self.external_id)
        return

    def _create_data(self, map_record, **kwargs):
        """ Get the data to pass to :py:meth:`_create` """
        return map_record.values(for_create=True, **kwargs)

    @contextmanager
    def _retry_unique_violation(self):
        """ Context manager: catch Unique constraint error and retry the
        job later.

        When we execute several jobs workers concurrently, it happens
        that 2 jobs are creating the same record at the same time
        (especially product templates as they are shared by a lot of
        sales orders), resulting in:

            IntegrityError: duplicate key value violates unique
            constraint "ayurdevas_product_product_external_id_uniq"
            DETAIL:  Key (backend_id, external_id)=(1, 4851) already exists.

        In that case, we'll retry the import just later.

        """
        try:
            yield
        except IntegrityError as err:
            if err.pgcode == errorcodes.UNIQUE_VIOLATION:
                raise RetryableJobError(
                    'A database error caused the failure of the job:\n'
                    '%s\n\n'
                    'Likely due to 2 concurrent jobs wanting to create '
                    'the same record. The job will be retried later.' % err)
            else:
                raise

    def _create(self, data):
        """ Create the OpenERP record """
        # special check on data before import
        self._validate_data(data)
        with self._retry_unique_violation():
            model_ctx = self.model.with_context(connector_no_export=True)
            binding = model_ctx.sudo().create(data)

        _logger.debug('%d created from backend %s', binding, self.external_id)
        return binding

    def _after_import(self, binding):
        """ Hook called at the end of the import """
        return

    def _import(self, binding):
        """ Import the external record.

        Can be inherited to modify for instance the environment
        (change current user, values in context, ...)

        """
        map_record = self._map_data()

        if binding:
            record = self._update_data(map_record)
            self._update(binding, record)
        else:
            record = self._create_data(map_record)
            binding = self._create(record)

        with self._retry_unique_violation():
            self.binder.bind(self.external_id, binding)

        self._after_import(binding)

    def run(self, external_id, force=False):
        """ Run the synchronization

        :param external_id: identifier of the record on Ayurdevas Ecommerce
        """
        self.external_id = external_id
        lock_name = 'import({}, {}, {}, {})'.format(
            self.backend_record._name,
            self.backend_record.id,
            self.work.model_name,
            external_id,
        )

        try:
            self.external_record = self._get_external_data()
        except IDMissingInBackend:
            return _('Record does no longer exist in backend')

        reason = self._must_skip(force=force)
        if reason:
            return reason

        binding = self._get_binding()

        if not force and self._is_uptodate(binding):
            return _('Already up-to-date.')

        # Keep a lock on this import until the transaction is committed
        # The lock is kept since we have detected that the informations
        # will be updated into Odoo
        self.advisory_lock_or_retry(lock_name)
        self._before_import()

        # import the missing linked resources
        self._import_dependencies()

        self._import(binding)


class BatchImporter(AbstractComponent):
    """ The role of a BatchImporter is to search for a list of
    items to import, then it can either import them directly or delay
    the import of each item separately.
    """

    _name = 'ayurdevas.batch.importer'
    _inherit = ['base.importer', 'ayurdevas.base']
    _usage = 'batch.importer'

    def run(self, filters=None):
        """ Run the synchronization """
        record_ids = self.backend_adapter.search(filters)
        for record_id in record_ids:
            self._import_record(record_id)

    def _import_record(self, external_id):
        """ Import a record directly or delay the import of the record.

        Method to implement in sub-classes.
        """
        raise NotImplementedError


class DirectBatchImporter(AbstractComponent):
    """ Import the records directly, without delaying the jobs. """

    _name = 'ayurdevas.direct.batch.importer'
    _inherit = 'ayurdevas.batch.importer'

    def _import_record(self, external_id):
        """ Import the record directly """
        self.model.import_record(self.backend_record, external_id)


class DelayedBatchImporter(AbstractComponent):
    """ Delay import of the records """

    _name = 'ayurdevas.delayed.batch.importer'
    _inherit = 'ayurdevas.batch.importer'

    def _import_record(self, external_id, job_options=None, **kwargs):
        """ Delay the import of the records"""
        delayable = self.model.with_delay(**job_options or {})
        delayable.import_record(self.backend_record, external_id, **kwargs)
