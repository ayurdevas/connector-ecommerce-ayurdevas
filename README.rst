=============================
Ayurdevas Ecommerce Connector
=============================


.. |badge1| image:: https://img.shields.io/badge/licence-AGPL--3-blue.png
    :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
    :alt: License: AGPL-3

|badge1|

Este modulo permite la comunicación de dos vias entre Odoo y el ecommerce de Ayurdevas.

Importación:

* Pedidos
* Clientes
* Pagos
* Direcciones de Entrega

Exportación:

* Clientes
* Estados de pedido (procesado/facturado/enviado/cancelado)
* Campañas
* Lineas de Puntos
* Lineas de Millas
* Saldos de cuenta corriente
* Producto (nombre, codigo, tipo, estado)


**Table of contents**

.. contents::
   :local:

Uso
=====
Configurar el backend, la información empezara a fluir sola.


Pagos
=====
Al importar los pedidos, si los mismos figuran pagos se crea un pago a cuenta
por el monto total para luego ser aplicado manualmente al emitir la factura.

Tener en cuenta que cuando se cancela un pedido, no hay restricción alguna si
este tiene un pago asociado, es responsabilidad del usuario darse cuenta.

Se agrega un comentario en el pago y el pedido para relacionarlos


Metodo de Envio
===============
En Odoo es necesario definir el campo `Ayurdevas Ecommerce Carrier Code` con el
mismo valor que exporta el API del ecommerce en el campo `shipping_method`.
De esa manera se relaciona la forma de envio web con la de Odoo


Equipo de Venta
===============
Es necesario completar el campo `Ecommerce Code` en los valores
correspondientes para poder exportar las campañas a la web


Lineas de Puntos
================
La misma se exporta al ser confirmada, lleva asociado el nro de pedido y la
campaña a la que corresponde.


Lineas de Millas
================
La misma se exporta al ser confirmada, lleva asociado el cliente y la
campaña correspondiente a la fecha.


Direcciones de Entrega
======================
Una dirección es un Partner con tipo 'Entrega'. Siempre habrá al menos una
dirección por partner. La heuristica para verificar direcciones existentes es
comprar los campos 'Correo electrónico', 'Calle' y 'Celular'. Si alguno de esos
campos cambia se crea una nueva dirección. La misma se carga como
Archivada para evitar sobrepoblar las busquedas de partners. La dirección de
entrega se asocia al pedido, la factura y el remito. Los datos de la dirección
de entrega salen impresos en el Remito y en la Factura Electrónica.


Actualizacion de stock Odoo a Ecommerce
=======================================
El metodo elegido de calculo es 'Stock Disponible Inmediato', esto es la
diferencia entre 'Cantidad a mano' y los movimientos de salida pendientes.
Adicionalmente este stock se calcula para los productos que están configurados
como 'Puede ser vendido' y no son de tipo 'Servicio' y se encuentran unicamente
en las ubicaciones de deposito seleccionadas en la configuración.
(Recepcion, Almacen, Picking, Entrega)

La actualización de stock se ejecuta cada 60 minutos mediante una
'Acción Planificada'. La misma recalcula el stock para cada producto, teniendo
en cuenta los parámetros anteriormente descriptos, y actualiza la web solamente
para los productos que sufireron modificaciones en su stock, esto evita
sobrecargar la web con actualizaciones innecesarias.
Otras maneras de ejecutar el recalculo de stock incluyen confirmar una
'Orden de Entrega' o una 'Recepción'. También se puede forzar una actualización
para un producto específico desde el menú 'Conectores / Ayurdevas / Productos'.

Cada 60 minutos: recalcular stock completo
Cada 'Orden de Entrega' confirmada: recalcular stock de productos involucrados


Known issues / Roadmap
======================
* Exportación completa de los datos del cliente
* Importación de productos
* Gestion de precios


Credits
=======

Authors
~~~~~~~

* Roberto Sierra <roberto@ideadigital.com.ar>

Maintainers
~~~~~~~~~~~

This module is maintained by Multidevas SA.
