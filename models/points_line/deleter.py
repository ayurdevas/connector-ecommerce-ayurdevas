# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).


from odoo.addons.component.core import Component


class AyurdevasPointsLineExporterDeleter(Component):
    """ PointsLine deleter """
    _name = 'ayurdevas.points.line.exporter.deleter'
    _inherit = 'ayurdevas.exporter.deleter'
    _apply_on = ['ayurdevas.points.line']
