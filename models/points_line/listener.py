# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.component_event import skip_if


class AyurdevasPointsLineExportListener(Component):
    _name = 'ayurdevas.points.line.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['points.line']

    def on_record_create(self, record, fields=None):
        for backend_id in self.env['ayurdevas.backend'].search([]):
            self.env['ayurdevas.points.line'].create({
                'backend_id': backend_id.id,
                'odoo_id': record.id
            })

    def on_record_write(self, record, fields=None):
        for binding in record.ayurdevas_bind_ids:
            if binding.backend_id.export_points_line:
                desc = _("Update points for partner %s") % record.partner_id.name
                binding.with_delay(description=desc).export_record()

    def on_record_unlink(self, record):
        for binding in record.ayurdevas_bind_ids:
            if binding.backend_id.export_points_line:
                with binding.backend_id.work_on(binding._name) as work:
                    external_id = work.component(usage='binder').to_external(binding)
                    if external_id:
                        binding.with_delay().export_delete_record(
                            binding.backend_id,
                            external_id
                        )


class AyurdevasBindingPointsLineExportListener(Component):
    _name = 'ayurdevas.binding.points.line.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['ayurdevas.points.line']

    @skip_if(lambda self, record, *args, **kwargs: not record.backend_id.export_points_line)
    def on_record_create(self, record, fields=None):
        desc = _("Export points for partner %s") % record.partner_id.name
        record.with_delay(description=desc).export_record()

    @skip_if(lambda self, record, *args, **kwargs: not record.backend_id.export_points_line)
    def on_record_unlink(self, record):
        with record.backend_id.work_on(record._name) as work:
            external_id = work.component(usage='binder').to_external(record)
            if external_id:
                record.with_delay().export_delete_record(
                    record.backend_id,
                    external_id
                )
