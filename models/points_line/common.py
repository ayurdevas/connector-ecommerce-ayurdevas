# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api, fields, models
from odoo.addons.component.core import Component


class AyurdevasPointsLine(models.Model):
    _name = 'ayurdevas.points.line'
    _inherit = 'ayurdevas.binding'
    _inherits = {'points.line': 'odoo_id'}
    _description = 'Ayurdevas Points Line'

    odoo_id = fields.Many2one(
        comodel_name='points.line',
        string='Product',
        required=True,
        ondelete='cascade'
    )


class PointsLine(models.Model):
    _inherit = 'points.line'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.points.line',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
        copy=False,
    )


class PointsLineAdapter(Component):
    _name = 'ayurdevas.points.line.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.points.line'

    def read(self, external_field, attributes=None):
        """ Returns the information of a record """
        with self.handle_404():
            record = self._call('GetPointsLine', external_field)
            return record

    def create(self, record):
        """ Create a record on the external system """
        return self._call('CreatePointsLine', record)['id']

    def write(self, external_id, data):
        """ Update records on the external system """
        self._call('UpdatePointsLine', external_id, data)

    def delete(self, external_id):
        """ Delete record """
        self._call('DeletePointsLine', external_id)
