# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _, fields
from odoo.tools import date_utils

from odoo.addons.component.core import Component
from odoo.addons.connector.components.mapper import mapping, m2o_to_external
from odoo.addons.connector.exception import InvalidDataError
from odoo.addons.points_miles.models.abstract_amount_line import MOTIVES


class AyurdevasPointsLineExporter(Component):
    """ Export points line to  Ecommerce """
    _name = 'ayurdevas.points.line.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _apply_on = 'ayurdevas.points.line'

    def _should_import(self):
        return False

    def _validate_create_data(self, data):
        """ Check if the values to export are correct

        Pro-actively check before the ``Model.create`` or
        ``Model.update`` if some fields are missing

        Raise `InvalidDataError`
        """
        if not data.get('sales_campaign_id'):
            raise InvalidDataError(_("Campaign is required. Please add a"
                                   " Campaign to the associated Invoice"
                                   ))
        return

    def _export_dependencies(self):
        invoice_line_ids = self.binding.invoice_line_id
        if invoice_line_ids:
            campaign_id = invoice_line_ids[0].invoice_id.campaign_id
        else:
            campaign_id = self.env['sales.campaign'].search_campaign(
                fields.Datetime.to_datetime(self.binding.date),
                self.binding.partner_id.team_id
            )
        if campaign_id:
            self._export_dependency(campaign_id, 'ayurdevas.sales.campaign')


class AyurdevasPointsLineExportMapper(Component):
    _name = 'ayurdevas.points.line.export.mapper'
    _inherit = 'ayurdevas.export.mapper'
    _apply_on = ['ayurdevas.points.line']

    direct = [
        ('qty', 'quantity'),
        (m2o_to_external('partner_id', 'ayurdevas.res.partner'), 'user_id'),
    ]

    @mapping
    def sale_order(self, record):
        data = {}
        invoice_line_ids = record.invoice_line_id
        if invoice_line_ids:
            invoice_id = invoice_line_ids[0].invoice_id
            data['sales_campaign_id'] = self._campaign_for_invoice(invoice_id)

            sale_external_id = self._sale_for_invoice_line(invoice_line_ids[0])
            if sale_external_id:
                data['purchase_id'] = sale_external_id
        else:
            # Si los puntos no vienen desde una factura se utiliza la fecha
            # del registro para calcular a que campaña pertenecen
            data['sales_campaign_id'] = self._campaign_from_date(record)

        return data

    @mapping
    def motive_value(self, record):
        for k, v in MOTIVES:
            if k == record.motive:
                return {'motive': v}
        return {'motive': record.motive}

    @mapping
    def status(self, record):
        value = 0
        if record.state == 'confirmed':
            value = 1
        return {'status': value}

    @mapping
    def date(self, record):
        return {'date': date_utils.json_default(record.date)}


    def _campaign_for_invoice(self, invoice_id):
        campaign_id = invoice_id.campaign_id
        if not campaign_id:
            return

        return self._campaign_to_external(campaign_id)

    def _sale_for_invoice_line(self, invoice_line_id):
        sale_line_ids = invoice_line_id.sale_line_ids
        if sale_line_ids:
            order_id = sale_line_ids[0].order_id
            binder = self.binder_for('ayurdevas.sale.order')
            return binder.to_external(order_id, wrap=True)

    def _campaign_from_date(self, record):
        campaign_id = self.env['sales.campaign'].search_campaign(
            fields.Datetime.to_datetime(record.date),
            record.partner_id.team_id
        )
        if campaign_id:
            return self._campaign_to_external(campaign_id.id)

    def _campaign_to_external(self, campaign_id):
        binder = self.binder_for('ayurdevas.sales.campaign')
        external_id = binder.to_external(campaign_id, wrap=True)
        assert external_id, (
            "Campaign %s should have been exported in "
            "AyurdevasPointsLineExporter._export_dependencies" % campaign_id.name
        )
        return external_id
