# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component


class AyurdevaAccountInvoiceListener(Component):
    _name = 'ayurdevas.account.invoice.listener'
    _inherit = 'base.event.listener'
    _apply_on = ['account.invoice']

    def on_invoice_paid(self, invoice):
        """ Export Sale Order paid status """
        sales = invoice.mapped('invoice_line_ids.sale_line_ids.order_id')
        for sale in sales:
            for binding in sale.ayurdevas_bind_ids:
                desc = _('Invoice paid for sales order %s') % (sale.name, )
                binding.with_delay(
                    description=desc
                ).export_state_change(state='invoice_paid')
