# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).


from odoo.addons.component.core import Component


class AyurdevasMilesLineExporterDeleter(Component):
    """ PointsLine deleter """
    _name = 'ayurdevas.miles.line.exporter.deleter'
    _inherit = 'ayurdevas.exporter.deleter'
    _apply_on = ['ayurdevas.miles.line']
