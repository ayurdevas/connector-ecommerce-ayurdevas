# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _, fields
from odoo.tools import date_utils

from odoo.addons.component.core import Component
from odoo.addons.connector.components.mapper import mapping, m2o_to_external
from odoo.addons.connector.exception import InvalidDataError
from odoo.addons.points_miles.models.abstract_amount_line import MOTIVES


class AyurdevasMilesLineExporter(Component):
    """ Export miles line to  Ecommerce """
    _name = 'ayurdevas.miles.line.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _apply_on = 'ayurdevas.miles.line'

    def _should_import(self):
        return False

    def _validate_create_data(self, data):
        """ Check if the values to export are correct

        Pro-actively check before the ``Model.create`` or
        ``Model.update`` if some fields are missing

        Raise `InvalidDataError`
        """
        if not data.get('user_id'):
            raise InvalidDataError(_('User is required'))
        if not data.get('sales_campaign_id'):
            raise InvalidDataError(_('Campaign is required'))
        return

    def _export_dependencies(self):
        invoice_id = self.binding.invoice_id
        if invoice_id:
            campaign_id = invoice_id.campaign_id
        else:
            campaign_id = self.env['sales.campaign'].search_campaign(
                fields.Datetime.to_datetime(self.binding.date),
                self.binding.partner_id.team_id
            )
        if campaign_id:
            self._export_dependency(campaign_id, 'ayurdevas.sales.campaign')


class AyurdevasMilesLineExportMapper(Component):
    _name = 'ayurdevas.miles.line.export.mapper'
    _inherit = 'ayurdevas.export.mapper'
    _apply_on = ['ayurdevas.miles.line']

    direct = [
        ('qty', 'quantity'),
        (m2o_to_external('partner_id', 'ayurdevas.res.partner'), 'user_id'),
    ]

    @mapping
    def sales_campaign(self, record):
        data = {}
        if record.invoice_id:
            data['sales_campaign_id'] = self._campaign_for_invoice(record.invoice_id)
        else:
            # Si los puntos no vienen desde una factura se utiliza la fecha
            # del registro para calcular a que campaña pertenecen
            data['sales_campaign_id'] = self._campaign_from_date(record)

        return data

    @mapping
    def motive_value(self, record):
        for k, v in MOTIVES:
            if k == record.motive:
                return {'motive': v}
        return {'motive': record.motive}

    @mapping
    def status(self, record):
        value = 0
        if record.state == 'confirmed':
            value = 1
        return {'status': value}

    @mapping
    def date(self, record):
        return {'date': date_utils.json_default(record.date)}

    def _campaign_for_invoice(self, invoice_id):
        campaign_id = invoice_id.campaign_id
        if not campaign_id:
            return

        return self._campaign_to_external(campaign_id)

    def _campaign_from_date(self, record):
        campaign_id = self.env['sales.campaign'].search_campaign(
            fields.Datetime.to_datetime(record.date),
            record.partner_id.team_id
        )
        if campaign_id:
            return self._campaign_to_external(campaign_id.id)

    def _campaign_to_external(self, campaign_id):
        binder = self.binder_for('ayurdevas.sales.campaign')
        external_id = binder.to_external(campaign_id, wrap=True)
        assert external_id, (
            "Campaign %s should have been exported in "
            "AyurdevasMilesLineExporter._export_dependencies" % campaign_id.name
        )
        return external_id
