# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import api, fields, models
from odoo.addons.component.core import Component


class AyurdevasMilesLine(models.Model):
    _name = 'ayurdevas.miles.line'
    _inherit = 'ayurdevas.binding'
    _inherits = {'miles.line': 'odoo_id'}
    _description = 'Ayurdevas Miles Line'

    odoo_id = fields.Many2one(
        comodel_name='miles.line',
        string='Product',
        required=True,
        ondelete='cascade'
    )


class MilesLine(models.Model):
    _inherit = 'miles.line'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.miles.line',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
        copy=False,
    )


class MilesLineAdapter(Component):
    _name = 'ayurdevas.miles.line.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.miles.line'

    def read(self, external_field, attributes=None):
        """ Returns the information of a record """
        with self.handle_404():
            record = self._call('GetMilesLine', external_field)
            return record

    def create(self, record):
        """ Create a record on the external system """
        return self._call('CreateMilesLine', record)['id']

    def write(self, external_id, data):
        """ Update records on the external system """
        self._call('UpdateMilesLine', external_id, data)

    def delete(self, external_id):
        """ Delete record """
        self._call('DeleteMilesLine', external_id)
