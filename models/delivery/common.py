# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import models, fields, api


class DeliveryCarrier(models.Model):
    """ Adds Ayurdevas Ecommerce specific fields to ``delivery.carrier``

    ``ayurdevas_code``

        Code of the carrier delivery method in Ayurdevas Ecommerce.
        Example: ``correo_argentino``
    """
    _inherit = "delivery.carrier"

    ayurdevas_code = fields.Char(
        string='Ayurdevas Ecommerce Carrier Code',
        required=False,
        groups='connector.group_connector_manager',
    )
