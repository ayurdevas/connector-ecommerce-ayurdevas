# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from . import ayurdevas_backend
from . import ayurdevas_binding
from . import account_invoice
from . import sale
from . import partner
from . import delivery
from . import product
from . import stock_picking
from . import points_line
from . import sales_team
from . import sales_campaign
from . import miles_line
