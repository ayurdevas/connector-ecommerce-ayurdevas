# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.connector.components.mapper import mapping
from odoo.addons.connector.exception import InvalidDataError


class ProductExporter(Component):
    """ Export product to Ayurdevas Ecommerce """
    _name = 'ayurdevas.product.product.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _apply_on = 'ayurdevas.product.product'

    def _should_import(self):
        return False

    def _validate_create_data(self, data):
        """ Check if the values to import are correct

        Pro-actively check before the ``Model.create`` or
        ``Model.update`` if some fields are missing

        Raise `InvalidDataError`
        """
        if not data.get('code'):
            raise InvalidDataError(_("Internal Reference required"))
        return


class ProductExportMapper(Component):
    _name = 'ayurdevas.product.product.export.mapper'
    _inherit = 'ayurdevas.export.mapper'
    _apply_on = ['ayurdevas.product.product']

    direct = [
        ('default_code', 'code'),
        ('name', 'name')
    ]

    @mapping
    def enabled(self, record):
    # Solo exportamos cuando el producto esta archivado. La activación debe ser
    # manual desde la web para tener un mayor control. Esto permite al
    # deposito operar sobre productos que estaban archivados sin que se
    # publiquen en la web
        if not record.active:
            return {'enabled': 0}

    @mapping
    def stock_type(self, record):
        if record.type == 'service':
            value = 2
        elif record.type == 'consu':
            value = 1
        else:  # record.type == 'product'
            value = 0

        return {'type': value}


class ProductInventoryExporter(Component):
    _name = 'ayurdevas.inventory.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _usage = 'product.inventory.exporter'
    _apply_on = 'ayurdevas.product.product'

    def _get_data(self, binding, fields):
        result = {}
        result['product_id'] = self.external_id
        if 'ecommerce_qty' in fields:
            result['quantity'] = binding.ecommerce_qty
        return result

    def run(self, binding, fields):
        """ Export the product inventory to Ecommerce """
        self.binding = binding

        self._export_dependencies()

        self.external_id = self.binder.to_external(binding)
        assert self.external_id, (
            "external_id should have been set in "
            "ProductInventoryExporter._export_dependencies")

        data = self._get_data(binding, fields)
        self.backend_adapter.update_inventory(data)

    def _export_dependencies(self):
        self._export_dependency(self.binding, 'ayurdevas.product.product')
