# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.component_event import skip_if


def should_skip(record):
    """
    To be used with :func:`odoo.addons.component_event.skip_if`
    on Events::

        from odoo.addons.component.core import Component
        from odoo.addons.component_event import skip_if


        class MyEventListener(Component):
            _name = 'my.event.listener'
            _inherit = 'base.connector.event.listener'
            _apply_on = ['magento.res.partner']

            @skip_if(lambda: self, record, *args, **kwargs:
                     should_skip(record))
            def on_record_write(self, record, fields=None):
                record.with_delay().export_record()

    """
    return not(record.sale_ok and record.default_code)


class AyurdevasProductExportListener(Component):
    _name = 'ayurdevas.product.product.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['product.product']

    # fields which should trigger an export of the product
    UPDATE_FIELDS = [
        'default_code', 
        'name',
        'active',
        'type',
    ]

    @skip_if(lambda self, record, *args, **kwargs: should_skip(record))
    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_write(self, record, fields=None):
        update_fields = list(
            set(fields).intersection(self.UPDATE_FIELDS)
        )
        if not update_fields:
            return

        for binding in record.ayurdevas_bind_ids:
            binding.with_delay(
                priority=20,
                description=_('Update product [%s] %s') % (record.default_code, record.name)
            ).export_record()

    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_create(self, record, fields=None):
        for backend_id in self.env['ayurdevas.backend'].search([]):
            self.env['ayurdevas.product.product'].create({
                'backend_id': backend_id.id,
                'odoo_id': record.id
            })


class AyurdevasBindingProductExportListener(Component):
    _name = 'ayurdevas.binding.product.product.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['ayurdevas.product.product']

    # fields which should not trigger an export of the products
    # but an export of their inventory
    INVENTORY_FIELDS = ['ecommerce_qty']

    @skip_if(lambda self, record, *args, **kwargs: should_skip(record))
    @skip_if(lambda self, record, **kwargs: self.no_connector_export(record))
    def on_record_write(self, record, fields=None):
        inventory_fields = list(
            set(fields).intersection(self.INVENTORY_FIELDS)
        )
        if inventory_fields:
            if record.no_stock_sync:
                return
            record.with_context(display_default_code=True).with_delay(
                priority=0,
                description=_('Export stock for [%s] %s') % (record.default_code, record.display_name)
            ).export_inventory(fields=inventory_fields)

    @skip_if(lambda self, record, *args, **kwargs: should_skip(record))
    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_create(self, record, fields=None):
        record.with_delay(
            priority=5,
            description=_('Export product [%s] %s') % (record.default_code, record.name)
        ).export_record()
