# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from collections import defaultdict
import requests

from odoo import _, api, fields, models
from odoo.addons.component.core import Component
from odoo.addons.queue_job.job import job, related_action
from odoo.addons.queue_job.exception import RetryableJobError


def chunks(items, length):
    for index in xrange(0, len(items), length):
        yield items[index:index + length]


class AyurdevasProductProduct(models.Model):
    _name = 'ayurdevas.product.product'
    _inherit = 'ayurdevas.binding'
    _inherits = {'product.product': 'odoo_id'}
    _description = 'Ayurdevas Product'

    odoo_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        required=True,
        ondelete='cascade'
    )

    no_stock_sync = fields.Boolean(
        string='No Stock Synchronization',
        help="Check this to exclude the product "
             "from stock synchronizations.",
    )

    ecommerce_qty = fields.Float(
        string='Computed Quantity',
        help="Last computed quantity to send "
             "on Ecommerce."
    )

    RECOMPUTE_QTY_STEP = 1000  # products at a time

    @api.multi
    def recompute_ecommerce_qty(self):
        """ Check if the quantity in the stock location configured
        on the backend has changed since the last export.

        If it has changed, write the updated quantity on `ecommerce_qty`.
        The write on `ecommerce_qty` will trigger an `on_record_write`
        event that will create an export job.

        It groups the products by backend to avoid to read the backend
        informations for each product.
        """
        # group products by backend
        backends = defaultdict(set)
        for product in self:
            backends[product.backend_id].add(product.id)

        for backend, product_ids in backends.items():
            self._recompute_ecommerce_qty_backend(
                backend,
                self.browse(product_ids)
            )
        return True

    @api.multi
    def _recompute_ecommerce_qty_backend(self, backend, products, read_fields=None):
        """ Recompute the products quantity for one backend.

        If field names are passed in ``read_fields`` (as a list), they
        will be read in the product that is used in
        :meth:`~._ecommerce_qty`.

        """
        if backend.product_stock_field_id:
            stock_field = backend.product_stock_field_id.name
        else:
            stock_field = 'virtual_available'

        location = self.env['stock.location']
        if self.env.context.get('location'):
            location = location.browse(self.env.context['location']).id
        elif backend.location_ids:
            location = backend.location_ids.ids
        else:
            location = backend.warehouse_id.lot_stock_id.id

        product_fields = ['ecommerce_qty', stock_field]
        if read_fields:
            product_fields += read_fields

        self_with_location = self.with_context(
            location=location,
            compute_child=True,
            force_company=backend.company_id.id,
        )
        for chunk_ids in chunks(products.ids, self.RECOMPUTE_QTY_STEP):
            records = self_with_location.browse(chunk_ids)
            for product in records.read(fields=product_fields):
                new_qty = self._ecommerce_qty(
                    product,
                    backend,
                    location,
                    stock_field
                )
                if new_qty != product['ecommerce_qty']:
                    self.browse(product['id']).ecommerce_qty = new_qty

    @api.multi
    def _ecommerce_qty(self, product, backend, location, stock_field):
        """ Return the current quantity for one product.

        Can be inherited to change the way the quantity is computed,
        according to a backend / location.

        If you need to read additional fields on the product, see the
        ``read_fields`` argument of :meth:`~._recompute_ecommerce_qty_backend`

        """
        return product[stock_field]

    @job(default_channel='root.ayurdevas')
    @related_action(action='related_action_unwrap_binding')
    @api.multi
    def export_inventory(self, fields=None):
        """ Export the inventory configuration and quantity of a product. """
        self.ensure_one()
        with self.backend_id.work_on(self._name) as work:
            exporter = work.component(usage='product.inventory.exporter')
            return exporter.run(self, fields)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.product.product',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
    )

    @api.model
    def load(self, fields, data):
        rslt = super(ProductProduct, self).load(fields, data)

        if 'import_file' in self.env.context:
            domain = [('id', 'in', rslt['ids'])]
            product_product_ids = self.search(domain)
            for product_product_id in product_product_ids:
                # No podemos saber si el registro se creo o modifico
                # por lo que disparo ambos eventos
                self._event('on_record_create').notify(product_product_id)
                self._event('on_record_write').notify(product_product_id)
        return rslt


class ProductProductAdapter(Component):
    _name = 'ayurdevas.product.product.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.product.product'

    def read(self, external_field, attributes=None):
        """ Returns the information of a record """
        with self.handle_404():
            record = self._call('GetProduct', external_field)
            return record

    def create(self, record):
        """ Create a record on the external system """
        try:
            return self._call('CreateProduct', record)['id']
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 404:
                message = _("Product not found [%s] %s.") % (record['code'], record['name'])
                raise RetryableJobError(message, seconds=3600)
            raise

    def write(self, external_id, data):
        """ Update records on the external system """
        with self.handle_404():
            self._call('UpdateProduct', external_id, data)

    def update_inventory(self, data):
        """ Update stock """
        with self.handle_404():
            self._call('UpdateStockQuant', data)
