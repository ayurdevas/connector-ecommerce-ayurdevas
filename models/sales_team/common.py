# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import models, fields, api


class CrmTeam(models.Model):

    """ Adds Ecommerce specific fields to ``crm.team``

    ``ayurdevas_code``

        Example: ``vtadta``
    """
    _inherit = 'crm.team'

    ayurdevas_code = fields.Char(
        string='Ecommerce Code',
        required=False,
        groups='connector.group_connector_manager',
    )
