# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import logging

from odoo import _, api, models, fields
from odoo.addons.component.core import Component
from odoo.addons.queue_job.job import job, related_action

from ...components.backend_adapter import AYURDEVAS_DATETIME_FORMAT

_logger = logging.getLogger(__name__)


class AyurdevasSaleOrder(models.Model):
    _name = 'ayurdevas.sale.order'
    _inherit = 'ayurdevas.binding'
    _description = 'Ayurdevas Ecommerce Sale Order'
    _inherits = {'sale.order': 'odoo_id'}

    odoo_id = fields.Many2one(
        comodel_name='sale.order',
        string='Sale Order',
        required=True,
        ondelete='cascade'
    )

    ayurdevas_order_id = fields.Integer(
        string='Ecommerce ID',
        help='Order ID in backend'
    )

    ayurdevas_order_line_ids = fields.One2many(
        comodel_name='ayurdevas.sale.order.line',
        inverse_name='ayurdevas_order_id',
        string='Order Lines'
    )

    external_reference = fields.Char(help='Order external reference')

    payment_method = fields.Char(size=32)

    date_paid = fields.Datetime(string='Paid Date')

    shipping_info = fields.Text()

    shipping_method = fields.Char()

    shipping_mail = fields.Char()

    shipping_cost = fields.Float(digits=(8, 2))

    total_amount = fields.Float(digits=(8, 2))

    total_points = fields.Float(digits=(8, 4))

    final_amount = fields.Float(digits=(8, 4))

    due_amount = fields.Float(digits=(8, 4))

    store_credit_amount = fields.Float(digits=(8, 2))

    paid_done = fields.Boolean()

    @job(default_channel='root.ayurdevas')
    @related_action(action='related_action_unwrap_binding')
    @api.multi
    def export_state_change(self, state=None):
        """
        Change state of a sales order on Ecommerce

        :param state: can be 'picking_done', 'invoice_paid' or 'sale_cancel'
        :type state: str
        """
        self.ensure_one()
        backend = self.backend_id
        if backend.no_sales_order_state_export:
            _logger.debug(
                "The backend '%s' is active "
                "but is configured not to export "
                "sale orders states", backend.name
            )
            return
        with backend.work_on(self._name) as work:
            exporter = work.component(usage='sale.state.exporter')
            return exporter.run(self, state=state)


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.sale.order',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
        copy=False,
    )

    points_used = fields.Float(
        string='Used Points',
        digits=(8, 2),
        track_visibility='onchange',
    )

    miles_used = fields.Float(
        string='Used Miles',
        digits=(8, 2),
        track_visibility='onchange',
    )

    def _ecommerce_cancel(self):
        """ Cancel sales order on Ecommerce """
        for order in self:
            if order.state == 'cancel':
                continue  # skip if already canceled
            for binding in order.ayurdevas_bind_ids:
                job_descr = _("Cancel sales order %s") % (order.name, )
                binding.with_delay(
                    description=job_descr
                ).export_state_change(state='sale_cancel')

    @api.multi
    def write(self, vals):
        if vals.get('state') == 'cancel':
            self._ecommerce_cancel()
        return super(SaleOrder, self).write(vals)


class AyurdevasSaleOrderLine(models.Model):
    _name = 'ayurdevas.sale.order.line'
    _inherit = 'ayurdevas.binding'
    _description = 'Ayurdevas Ecommerce Sale Order Line'
    _inherits = {'sale.order.line': 'odoo_id'}

    ayurdevas_order_id = fields.Many2one(
        comodel_name='ayurdevas.sale.order',
        string='ayurdevas Sale Order',
        required=True,
        ondelete='cascade',
        index=True
    )

    odoo_id = fields.Many2one(
        comodel_name='sale.order.line',
        string='Sale Order Line',
        required=True,
        ondelete='cascade'
    )

    backend_id = fields.Many2one(
        related='ayurdevas_order_id.backend_id',
        string='ayurdevas Backend',
        readonly=True,
        store=True,
        # override 'ayurdevas.binding', can't be INSERTed if True:
        required=False,
    )

    @api.model
    def create(self, vals):
        ayurdevas_order_id = vals['ayurdevas_order_id']
        order_binding = self.env['ayurdevas.sale.order'].browse(ayurdevas_order_id)
        vals['order_id'] = order_binding.odoo_id.id
        binding = super(AyurdevasSaleOrderLine, self).create(vals)
        return binding


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.sale.order.line',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
    )


class SaleOrderAdapter(Component):
    _name = 'ayurdevas.sale.order.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.sale.order'

    def search(self, from_date=None, to_date=None):
        """ Search records according to some criterias
        and returns a list of ids

        :rtype: list
        """
        filters = {}
        dt_fmt = AYURDEVAS_DATETIME_FORMAT
        if from_date is not None:
            filters['from_date'] = from_date.strftime(dt_fmt)
        if to_date is not None:
            filters['to_date'] = to_date.strftime(dt_fmt)
        return self._call('GetPendingOrders', filters)

    def read(self, sale_id, attributes=None):
        """ Returns the information of a record

        :rtype: dict
        """
        with self.handle_404():
            record = self._call('GetOrder', sale_id)
            return record

    def update(self, sale_id, params):
        """ Update sale order attributes """
        with self.handle_404():
            self._call('UpdateOrder', sale_id, params)
