# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component


class StateExporter(Component):
    _name = 'ayurdevas.sale.state.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _usage = 'sale.state.exporter'
    _apply_on = 'ayurdevas.sale.order'

    ORDER_STATUS_MAPPING = {
        'picking_done': 'processed',
        'invoice_paid': 'paid',
        'sale_cancel': 'cancelled'
    }

    def run(self, binding, state=None):
        """ Change the status of the sales order on Ecommerce.

        :param binding: the binding record of the sales order
        :param state: state for export
        :type state: str
        """
        if state and state not in StateExporter.ORDER_STATUS_MAPPING:
            return _('State %s is not exported.') % state
        external_id = self.binder.to_external(binding)
        if not external_id:
            return _('Sale is not linked with a Ayurdevas sales order')
        ayurdevas_state = self.ORDER_STATUS_MAPPING[state]
        params = {ayurdevas_state: True}
        self.backend_adapter.update(external_id, params)

    def _should_import(self):
        return False
