# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from datetime import timedelta, timezone
import logging

from odoo import fields, _
from odoo.tools import float_is_zero
from odoo.addons.component.core import Component
from odoo.addons.connector.components.mapper import mapping, none
from ...components.mapper import normalize_datetime

_logger = logging.getLogger(__name__)


class SaleOrderImportMapper(Component):
    """
    sales = {
        "data": {
        "id":150610,
        "user_id":2414,
        "order_ref":"400067673",
        "payment_method":"tarjeta",
        "external_reference":"7b323173e0f1d22c5dd3e6fde61e6eb8",
        "shipping_info": {
            "calle": "",
            "numero": "",
            "piso": "",
            "depto": "",
            "cp": "",
            "ciudad": "",
            "provincia": "",
            "mail": "",
            "telefono": "",
            "nombre": "",
            "destinatario": "",
            "dni": "",
        },
        "shipping_method":"compra_compartida",
        "shipping_mail":"foo@bar.com",
        "shipping_cost":"300.00",
        "saldo":"0.0",
        "importe_final":"2023.56",
        "total_monto":"1723.56",
        "total_puntos":"3.0700",
        "created_at":"2020-06-25 14:59:23",
        "cancelled":0,
        "processed":0,
        "paid":1,
        "paid_at": "2020-06-25 14:58:00",
        "status":"approved (accredited)",
        "items":{
            "data": [
                {
                    "line_id":1196437,
                    "product_id":12371,
                    "quantity":1,
                    "discount":40.92,
                    "price_unit_incl_tax":750,
                    "row_total":443.14,
                    "points":0.78,
                    "kit_item":358294
                },
                {
                    "line_id":1196438,
                    "product_id":4817,
                    "quantity":1,
                    "discount":40.92,
                    "price_unit_incl_tax":899,
                    "row_total":531.17,
                    "points":0.94,
                    "kit_item":358294
                },
                {
                    "line_id":1196439,
                    "product_id":25439,
                    "quantity":1,
                    "discount":25,
                    "price_unit_incl_tax":499,
                    "row_total":374.25,
                    "points":0.68,
                    "kit_item":0
                },
                {"line_id":1196440,
                    "product_id":25236,
                    "quantity":1,
                    "discount":25,
                    "price_unit_incl_tax":500,
                    "row_total":375,
                    "points":0.67,
                    "kit_item":0
                }
            ]}
        }
    }
    """
    _name = 'ayurdevas.sale.order.mapper'
    _inherit = 'ayurdevas.import.mapper'
    _apply_on = 'ayurdevas.sale.order'

    direct = [
        ('id', 'external_id'),
        ('external_reference','external_reference'),
        (normalize_datetime('created_at'), 'date_order'),
        (none(normalize_datetime('paid_at')), 'date_paid'),
        ('payment_method','payment_method'),
        ('shipping_cost','shipping_cost'),
        ('shipping_info','shipping_info'),
        ('shipping_mail','shipping_mail'),
        ('shipping_method','shipping_method'),
        ('total_monto','total_amount'),
        ('total_puntos','total_points'),
        ('miles_used','miles_used'),
        ('points_used','points_used'),
        ('importe_final', 'final_amount'),
        ('saldo', 'due_amount'),
        ('store_credit', 'store_credit_amount'),
        ('paid', 'paid_done'),
    ]

    children = [
        ('items', 'ayurdevas_order_line_ids', 'ayurdevas.sale.order.line'),
    ]

    @mapping
    def name(self, record):
        name = record['order_ref']
        prefix = self.backend_record.sale_prefix
        if prefix:
            name = prefix + name
        return {'name': name}

    @mapping
    def delivery_carrier(self, record):
        ifield = record.get('shipping_method')
        if not ifield:
            return

        carrier = self.env['delivery.carrier'].search(
            [('ayurdevas_code', '=', ifield)],
            limit=1,
        )

        assert carrier, (
            "Missing carrier with backend code %s. Please create or configure"
            " the record first." % ifield
        )

        return {'carrier_id': carrier.id}

    @mapping
    def backend_id(self, record):
        return {'backend_id': self.backend_record.id}

    @mapping
    def user_id(self, record):
        """ Do not assign to a Salesperson otherwise sales orders are hidden
        for the salespersons (access rules)"""
        return {'user_id': False}

    @mapping
    def customer_id(self, record):
        binder = self.binder_for('ayurdevas.res.partner')
        partner = binder.to_internal(record['user_id'], unwrap=True)
        assert partner, (
            "user_id %s should have been imported in "
            "SaleOrderImporter._import_dependencies" % record['user_id'])
        return {'partner_id': partner.id}

    def _add_shipping_line(self, map_record, values):
        record = map_record.source
        amount_incl = float(record.get('shipping_cost') or 0.0)
        line_builder = self.component(usage='order.line.builder.shipping')
        line_builder.price_unit = amount_incl

        if values.get('carrier_id'):
            carrier = self.env['delivery.carrier'].browse(values['carrier_id'])
            line_builder.product = carrier.product_id
            line = (0, 0, line_builder.get_line())
            values['order_line'].append(line)

        return values

    def finalize(self, map_record, values):
        values.setdefault('order_line', [])
        values = self._add_shipping_line(map_record, values)
        values.update(self._mapper_options())
        # Forces values from configs, overriding defined values in partner.
        # This is specially usefull for sales made by guest users to registered
        # partners. Allowing to set correct values for fields like: sale_type,
        # pricelist and sale_campaing
        values.update(self._sale_type_fields(map_record))
        onchange = self.component(usage='ecommerce.onchange.manager.sale.order')
        return onchange.play(values, values['ayurdevas_order_line_ids'])

    @mapping
    def status(self, record):
        cancelled = bool(record['cancelled'] or False)
        if cancelled:
            return {'state': 'cancel'}

    def _mapper_options(self):
        return {
            'partner_id': self.options.partner_id,
            'partner_invoice_id': self.options.partner_invoice_id,
            'partner_shipping_id': self.options.partner_shipping_id,
        }

    def _sale_type_fields(self, map_record):
        record = map_record.source
        sale_type = record.get('sale_type')
        config = self.backend_record.find_partner_configuration(sale_type)
        if not config:
            raise AssertionError(
                'Missing Partner configuration for type "%s". '
                'Please create or configure the record' % type_desc
            )

        return {
            'payment_term_id': config.payment_term_id.id,
            'pricelist_id': config.pricelist_id.id,
            'type_id': config.sale_type_id.id,
            'team_id': config.team_id.id,
        }


class SaleOrderLineImportMapper(Component):

    _name = 'ayurdevas.sale.order.line.mapper'
    _inherit = 'ayurdevas.import.mapper'
    _apply_on = 'ayurdevas.sale.order.line'

    direct = [
        ('line_id', 'external_id'),
        ('quantity', 'product_uom_qty'),
        ('quantity', 'product_qty'),
    ]

    @mapping
    def product_id(self, record):
        binder = self.binder_for('ayurdevas.product.product')
        product = binder.to_internal(record['product_id'], unwrap=True)
        assert product, (
            "product_id %s does not exist. Please create the record first."
            % record['product_id']
        )
        return {'product_id': product.id}

    @mapping
    def discount(self, record):
        discount = float(record.get('discount') or 0.)
        return {'discount': discount}

    @mapping
    def price(self, record):
        price_unit_incl_tax = float(record['price_unit_incl_tax'] or 0.)
        return {'price_unit': price_unit_incl_tax}

    @mapping
    def points(self, record):
        total_points = float(record['points'])
        qty_ordered = float(record['quantity'])
        points_per_unit = round(total_points / qty_ordered, 2)
        return {'points': points_per_unit}

    def finalize(self, map_record, values):
        record = map_record.source
        price = float(record['price_unit_incl_tax'] or 0.)
        used_points = bool(record['points_used'] or 0)
        used_miles = bool(record['miles_used'] or 0)
        # Export invoices require that all products have price
        if float_is_zero(price, 2) or used_points or used_miles:
            values['price_unit'] = 0.01
            values['discount'] = 100
        return values


class SaleOrderBatchImporter(Component):
    _name = 'ayurdevas.sale.order.batch.importer'
    _inherit = 'ayurdevas.delayed.batch.importer'
    _apply_on = 'ayurdevas.sale.order'

    def _import_record(self, external_id, job_options=None, **kwargs):
        job_options = {
            'max_retries': 3,
            'priority': 5,
            'description': _('Import Sale Order'),
        }

        return super(SaleOrderBatchImporter, self)._import_record(
            external_id,
            job_options=job_options
        )

    def run(self, filters=None):
        """ Run the synchronization """
        if filters is None:
            filters = {}
        from_date = filters.pop('from_date', None)
        to_date = filters.pop('to_date', None)
        external_ids = self.backend_adapter.search(
            from_date=from_date,
            to_date=to_date
        )
        _logger.info('search for ayurdevas saleorders %s returned %s',
                     filters, external_ids)
        for external_id in external_ids:
            self._import_record(external_id)


class SaleOrderImporter(Component):
    _name = 'ayurdevas.sale.order.importer'
    _inherit = 'ayurdevas.base.importer'
    _apply_on = 'ayurdevas.sale.order'

    def _must_skip(self, force=False):
        skip = super(SaleOrderImporter, self)._must_skip(force=force)
        if skip:
            return skip
        if self.binder.to_internal(self.external_id):
            return _('Already imported')

    def _compute_parent_item(self, item_id, items):
        line_item = {}
        line_item['product_id'] = item_id
        line_item['parent_item_id'] = 0
        line_item['line_id'] = min([i['line_id'] for i in items])
        line_item['points_used'] = 0
        line_item['miles_used'] = 0

        quantity = min([i['quantity'] for i in items])
        line_item['quantity'] = quantity

        total_points = sum([i['points'] for i in items])
        line_item['points'] = total_points / quantity

        price_incl_tax = sum([i['price_unit_incl_tax'] for i in items])
        line_item['price_unit_incl_tax'] = price_incl_tax

        row_total = sum([i['row_total'] / i['quantity'] for i in items])
        line_item['row_total'] = row_total

        if price_incl_tax:
            perc = round((row_total * 100.) / price_incl_tax, 3)
            line_item['discount'] = 100. - perc

        return line_item

    def _remove_child_items(self, resource):
        """ Removes lines that are part of a product kit """
        child_items = {}  # key is the kit item id
        all_items = []

        # Group the childs with their parent
        for item in resource['items']:
            if item.get('parent_item_id'):
                child_items.setdefault(item['parent_item_id'], []).append(item)
            else:
                all_items.append(item)

        for parent_item_id, childs in child_items.items():
            line_item = self._compute_parent_item(parent_item_id, childs)
            all_items.append(line_item)

        resource['items'] = all_items
        return resource

    def _remove_items_data_key(self, record):
        order_lines = record['items'].get('data', [])
        record['items'] = order_lines
        return record

    def _get_external_data(self):
        record = super(SaleOrderImporter, self)._get_external_data()
        record = self._remove_items_data_key(record)
        return self._remove_child_items(record)

    def _import_dependencies(self):
        partner_binding = self._import_partner()
        partner_binder = self.binder_for('ayurdevas.res.partner')
        partner_id = partner_binder.unwrap_binding(partner_binding)
        self.partner_id = partner_id.id
        self.partner_invoice_id = partner_id.id

        shipping_binding = self._import_addresses(partner_id, partner_binding)
        shipping_binder = self.binder_for('ayurdevas.address')
        shipping_id = shipping_binder.unwrap_binding(shipping_binding)
        if shipping_id:
            self.partner_shipping_id = shipping_id.id
        else:
            self.partner_shipping_id = partner_id.id

    def _import_addresses(self, partner_id, partner_binding):
        # We always import the addresses for each sale order,
        # because the ecommerce does not provide a ``customer_address_id``

        # We import the addresses of the sale order as Active = False
        # so they will be available in the documents generated as the
        # sale order or the picking, but they won't be available on
        # the partner form or the searches. Too many adresses would
        # be displayed.
        # They are never synchronized.
        record = self.external_record

        def is_empty(shipping_record):
            return not (shipping_record.get('calle', '').strip()
                and shipping_record.get('numero', '').strip()
                and shipping_record.get('ciudad', '').strip()
            )

        # Ecommerce always sends shipping information even if its empty
        if is_empty(record['shipping_info']):
            return

        address_record = record['shipping_info']

        address_mapper = self.component(
            usage='import.mapper',
            model_name='ayurdevas.address'
        )
        map_record = address_mapper.map_record(address_record)
        map_record.update({
            'parent_id': partner_id.id,
            'ayurdevas_partner_id': partner_binding.id,
            'active': False,
            'is_ecommerce_order_address': True,
        })
        vals = map_record.values(for_create=True, parent_partner=partner_id)

        ayurdevas_address = self.env['ayurdevas.address'].with_context(active_test=False)
        address_binding = ayurdevas_address.search([
            ('parent_id', '=', partner_id.id),
            ('ayurdevas_partner_id', '=', partner_binding.id),
            ('email', '=', vals['email']),
            ('street', '=', vals['street']),
            ('mobile', '=', vals['mobile']),
        ], limit=1)
        if not address_binding:
            address_binding = ayurdevas_address.create(vals)
        return address_binding

    def _import_partner(self):
        record = self.external_record

        # Ecommerce allows to create a sale order not registered as a user
        is_guest_order = bool(int(record.get('user_id', 0)) == 0)
        binder = self.binder_for('ayurdevas.res.partner')
        if not is_guest_order:
            # Partner created/updated from web or legacy system
            self._import_dependency(record['user_id'], 'ayurdevas.res.partner')
            partner_binding = binder.to_internal(record['user_id'])
        else:
            partner_binding = self._search_guest_partner(record)
            if not partner_binding:
                partner_binding = self._import_guest_partner(record)
            record['user_id'] = binder.to_external(partner_binding)
        return partner_binding

    def _search_guest_partner(self, record):
        """ Search an existing guest partner by VAT number """
        vat = record['shipping_info']['dni']
        domain = [
            ('vat', '=', vat),
            ('backend_id', '=', self.backend_record.id),
            ('type', '!=', 'delivery'),  # Do not consider delivery addresses
        ]
        # Because a Guest user does not register the partner could be
        # inactive in Odoo and still make orders
        partner_ctx = self.env['ayurdevas.res.partner'].with_context(
            active_test=False
        )
        return partner_ctx.search(domain, limit=1)

    def _import_guest_partner(self, record):
        address = record['shipping_info']

        customer_record = {
            'confirmed': 1,
            'email': record['shipping_mail'],
            'name': address['nombre'],
            'vat': address['dni'],
            'mobile': address['telefono'],
            'fiscal_position': 5,  # consumidor final
            'document_type': 96,  # dni
            'type': 'ecommerce',
            'created_at': record['created_at'],
            'updated_at': False,
            'start_date': False,
            'end_date': False,
            'restart_date': False,
        }

        mapper = self.component(usage='import.mapper',
                                model_name='ayurdevas.res.partner')
        map_record = mapper.map_record(customer_record)
        map_record.update({'ayurdevas_ecommerce_access': False})

        # In case guest partner does not have a complete address we set
        # the state to 'CABA'
        if not address.get('provincia'):
            state_id = self.env.ref('base.state_ar_c')
            map_record.update({'state': state_id.name})

        partner_ctx = self.env['ayurdevas.res.partner'].with_context(
            connector_no_export=True
        )
        partner_binding = partner_ctx.create(
            map_record.values(for_create=True)
        )
        partner_binder = self.binder_for('ayurdevas.res.partner')
        partner_binder.bind(self._compute_guest_id(record), partner_binding)
        return partner_binding

    def _compute_guest_id(self, record):
        return 'guestorder: %s' % record['shipping_info']['dni'].strip()

    def _after_import(self, binding):
        """ Import payments """
        if self._should_register_payment(binding):
            self._register_payment(binding)

    def _should_register_payment(self, binding):
        return (binding.paid_done and
            self.backend_record.should_import_payment(binding.payment_method))

    def _register_payment(self, binding):
        Payment = self.env['account.payment']
        pay = self._prepare_payment(binding)

        new_payment = Payment.sudo().create(pay)
        new_payment.post_l10n_ar()
        message = _("This payment has been created from the sale order: <a href=# data-oe-model=sale.order data-oe-id=%d>%s</a>") % (binding.odoo_id, binding.name)
        new_payment.message_post(body=message)
        sale = binding.odoo_id
        message = _("A payment has been created for this sale order: <a href=# data-oe-model=account.payment data-oe-id=%d>%s</a>") % (new_payment.id, new_payment.name)
        sale.message_post(body=message)

    def _prepare_payment(self, binding):
        payment_method_id = self.backend_record.payment_method_id
        journal_id = self.backend_record.journal_id
        payment_type_id = self.backend_record.account_payment_type_id
        pos_ar_id = self.backend_record.pos_ar_id

        return {
            'amount': binding.final_amount,
            'payment_date': binding.date_paid,
            'communication': binding.name,
            'partner_id': binding.partner_id.id,
            'partner_type': 'customer',
            'payment_type': 'inbound',
            'payment_method_id': payment_method_id.id,
            'journal_id': journal_id.id,
            'pos_ar_id': pos_ar_id.id,
            'payment_type_line_ids': [(0, 0, {
                'account_payment_type_id': payment_type_id.id,
                'amount': binding.final_amount,
            })],
        }

    def _check_special_fields(self):
        assert self.partner_id, (
            "self.partner_id should have been defined "
            "in SaleOrderImporter._import_partner")
        assert self.partner_invoice_id, (
            "self.partner_id should have been "
            "defined in SaleOrderImporter._import_addresses")
        assert self.partner_shipping_id, (
            "self.partner_id should have been defined "
            "in SaleOrderImporter._import_addresses")

    def _create_data(self, map_record, **kwargs):
        self._check_special_fields()
        return super(SaleOrderImporter, self)._create_data(
            map_record,
            partner_id=self.partner_id,
            partner_invoice_id=self.partner_invoice_id,
            partner_shipping_id=self.partner_shipping_id,
            **kwargs
        )

    def _update_data(self, map_record, **kwargs):
        self._check_special_fields()
        return super(SaleOrderImporter, self)._update_data(
            map_record,
            partner_id=self.partner_id,
            partner_invoice_id=self.partner_invoice_id,
            partner_shipping_id=self.partner_shipping_id,
            **kwargs
        )
