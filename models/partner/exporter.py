# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.connector.exception import InvalidDataError
from odoo.addons.connector.components.mapper import (
    changed_by,
    mapping,
    only_create,
    none
)


class ResPartnerExporter(Component):
    """ Export Partner to Ayurdevas Ecommerce """
    _name = 'ayurdevas.res.partner.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _apply_on = 'ayurdevas.res.partner'

    def _validate_create_data(self, data):
        """ Check if the values to import are correct

        Pro-actively check before the ``Model.create`` or
        ``Model.update`` if some fields are missing

        Raise `InvalidDataError`
        """
        if not data.get('username'):
            raise InvalidDataError(_("Internal Reference required"))
        if not data.get('presea_dni'):
            raise InvalidDataError(_("VAT number required"))
        if not(data.get('categoria') and data.get('presea_tipo')):
            raise InvalidDataError(_("Sales team is not defined or invalid"))
        return

    def _should_import(self):
        return False

    def _has_to_skip(self):
        """ Skip exporting guest partners"""
        if self.external_id and str(self.external_id).startswith('guestorder:'):
            return True
        return super(ResPartnerExporter, self)._has_to_skip()


class ResPartnerExportMapper(Component):
    _name = 'ayurdevas.partner.export.mapper'
    _inherit = 'ayurdevas.export.mapper'
    _apply_on = ['ayurdevas.res.partner']

    direct = [
        ('ref', 'username'),
        ('name','presea_n_fantasia'),
        (none('email'), 'email'),
        ('vat', 'presea_dni')
    ]

    @mapping
    def confirmed(self, record):
        return {'confirmed': record.active and record.ayurdevas_ecommerce_access}

    @changed_by('property_account_position_id')
    @mapping
    def presea_iva(self, record):
        tax_exclusion = False
        excluded_account = self.env.ref('l10n_ar_afip_tables.account_fiscal_position_cliente_ext')
        if excluded_account == record.property_account_position_id:
            tax_exclusion = True
        return {'presea_iva': tax_exclusion}

    @only_create
    @mapping
    def password(self, record):
        return {'presea_clave': record.ayurdevas_ecommerce_password}

    @changed_by('team_id')
    @mapping
    def categoria(self, record):
        cat = ''
        sales_team_ref = self._get_sales_team_ref(record)

        if sales_team_ref in ['connector_ecommerce_ayurdevas.crm_team_dn', 'connector_ecommerce_ayurdevas.crm_team_dnp']:
            cat = 'MULT'
        elif sales_team_ref == 'connector_ecommerce_ayurdevas.crm_team_venta_directa':
            cat = 'VTADTA'
            ppp = record.property_product_pricelist
            pricelist_ref = ppp.get_external_id()[ppp.id]
            if pricelist_ref == 'connector_ecommerce_ayurdevas.product_pricelist_distribuidor_35':
                cat = 'LIDERES'
        elif sales_team_ref in ['connector_ecommerce_ayurdevas.crm_team_franquicia', 'connector_ecommerce_ayurdevas.crm_team_empleados']:
            cat = 'FRANQ'
        elif sales_team_ref == 'connector_ecommerce_ayurdevas.crm_team_nuevo_point':
            cat = 'NUEVOPOINT'
        elif sales_team_ref == 'connector_ecommerce_ayurdevas.crm_team_point':
            cat = 'POINT'
        return {'categoria': cat}

    @changed_by('ref')
    @mapping
    def presea_franquicia(self, record):
        num = 99999
        if record.ref and record.ref.isdigit():
            num = int(record.ref)
        return {'presea_franquicia': num < 11000 }

    @changed_by('team_id')
    @mapping
    def presea_tipo(self, record):
        tipo = 0
        sales_team_ref = self._get_sales_team_ref(record)

        if sales_team_ref in ['connector_ecommerce_ayurdevas.crm_team_dn','connector_ecommerce_ayurdevas.crm_team_dnp','connector_ecommerce_ayurdevas.crm_team_empleados']:
            tipo = 1
        elif sales_team_ref == 'connector_ecommerce_ayurdevas.crm_team_point':
            tipo = 2
        elif sales_team_ref in ['connector_ecommerce_ayurdevas.crm_team_venta_directa']:
            tipo = 3
        elif sales_team_ref == 'connector_ecommerce_ayurdevas.crm_team_franquicia':
            tipo = 4
        return {'presea_tipo':tipo}

    @mapping
    def compute_red(self, record):
        red = {
            'presea_red': '',
            'presea_red2': '',
            'presea_red3': ''
        }
        if record.sponsor_id:
            red['presea_red3'] = record.sponsor_id.ref
            sponsor_id2 = record.sponsor_id.sponsor_id
            if sponsor_id2:
                red['presea_red2'] = sponsor_id2.ref
        elif record.leader_id:
            red['presea_red2'] = record.leader_id.ref
        return red

    def _get_sales_team_ref(self, record):
        if record.team_id:
            return record.team_id.get_external_id()[record.team_id.id]
        return None

    @mapping
    def account_balance(self, record):
        # Invertir el signo del saldo, ya que para nosotros la nuestra deuda es
        # positiva pero al cliente se muestra en negativo
        due = float(record['ecommerce_total_due']) or 0
        return {'account_balance': -1 * due}
