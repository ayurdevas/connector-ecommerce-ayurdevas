# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import secrets
import string
from collections import defaultdict

from odoo import api, fields, models, _
from odoo.addons.component.core import Component


def chunks(items, length):
    for index in xrange(0, len(items), length):
        yield items[index:index + length]


class AyurdevasResPartner(models.Model):
    _name = 'ayurdevas.res.partner'
    _inherit = 'ayurdevas.binding'
    _description = 'Ayurdevas Ecommerce Partner'
    _inherits = {'res.partner': 'odoo_id'}

    odoo_id = fields.Many2one(
        comodel_name='res.partner',
        string='Partner',
        required=True,
        ondelete='cascade'
    )

    ecommerce_total_due = fields.Monetary(
        string='Due',
        readonly=True
    )

    RECOMPUTE_QTY_STEP = 1000  # partners at a time

    @api.multi
    def recompute_ecommerce_total_due(self):
        """ Check if the amount in total_due has changed since the last
        export.

        If it has changed, write the updated quantity on
        `ecommerce_total_due`. The write on `ecommerce_total_due` will
        trigger an `on_record_write` event that will create an export
        job.

        It groups the partners by backend to avoid reading the backend
        information for each partner.
        """
        # group partners by backend
        backends = defaultdict(set)
        for partner in self:
            backends[partner.backend_id].add(partner.id)

        for backend, partner_ids in backends.items():
            self._recompute_ecommerce_total_due_backend(
                backend,
                self.browse(partner_ids)
            )
        return True

    @api.multi
    def _recompute_ecommerce_total_due_backend(self, backend, partners,
                                               read_fields=None):
        """ Recompute the partners due amount for one backend.

        If field names are passed in ``read_fields`` (as a list), they
        will be read in the partner that is used in
        :meth:`~._ecommerce_total_due`.
        """
        due_field = 'total_due'
        partner_fields = ['ecommerce_total_due', due_field]
        if read_fields:
            partner_fields += read_fields

        for chunk_ids in chunks(partners.ids, self.RECOMPUTE_QTY_STEP):
            records = self.browse(chunk_ids)
            for partner in records.read(fields=partner_fields):
                new_total_due = self._ecommerce_total_due(
                    partner,
                    backend,
                    due_field
                )
                if new_total_due != partner['ecommerce_total_due']:
                    self.browse(partner['id']).ecommerce_total_due = new_total_due

    @api.multi
    def _ecommerce_total_due(self, partner, backend, due_field):
        """ Return the current amount due for one partner.

        Can be inherited to change the way the amount is computed,
        according to a backend.

        If you need to read additional fields on the partner, see the
        ``read_fields`` argument of
        :meth:`~._recompute_ecommerce_total_due_backend`
        """
        return partner[due_field]

    @api.multi
    def update_due(self):
        """
        Compute the field 'total_due'

        Based on account_reports/models/res_partner.py#_compute_for_followup
        """
        field_names = ['amount_residual:sum']
        groupby = ['partner_id']
        domain_due = [
            ('partner_id', 'in', self.ids),
            ('reconciled', '=', False),
            ('account_id.deprecated', '=', False),
            ('account_id.internal_type', '=', 'receivable'),
            ('move_id.state', '=', 'posted'),
        ]
        total_due_all = self.env['account.move.line'].read_group(
            domain_due,
            field_names,
            groupby,
        )
        total_due_all = dict(
            (res['partner_id'][0], res['amount_residual'])
            for res in total_due_all
        )

        for record in self:
            total_due = total_due_all.get(record.id, 0)
            record.total_due = total_due


class ResPartner(models.Model):
    _inherit = 'res.partner'

    def _default_ecommerce_password(self):
        alphabet = string.ascii_letters + string.digits
        while True:
            password = ''.join(secrets.choice(alphabet) for i in range(8))
            if sum(c.isdigit() for c in password) >= 4:
                return password.upper()

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.res.partner',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
    )

    ayurdevas_address_bind_ids = fields.One2many(
        comodel_name='ayurdevas.address',
        inverse_name='odoo_id',
        string="Ayurdevas Address Bindings",
    )

    ayurdevas_ecommerce_password = fields.Char(
        string='Ecommerce Password',
        help='Partner password for ecommerce',
        size=8,
        copy=False,
        readonly=True,
        default=_default_ecommerce_password
    )

    ayurdevas_ecommerce_access = fields.Boolean(
        string='Ecommerce Access',
        help='Enables partner access to ecommerce',
        track_visibility='onchange',
    )

    def _disable_ecommerce_login(self):
        """ Disable user login on Ecommerce """
        for partner in self:
            # Only root partner can be sync'ed
            if partner.parent_id:
                return
            for binding in partner.ayurdevas_bind_ids:
                job_descr = _("Partner access disabled for %s") % (binding.external_id,)
                binding.with_delay(
                    description=job_descr
                ).export_record()

    @api.multi
    def write(self, vals):
        active = vals.get('active', True)
        eaccess = vals.get('ayurdevas_ecommerce_access', True)
        if not(active and eaccess):
            self._disable_ecommerce_login()
        return super(ResPartner, self).write(vals)

    @api.model
    def _commercial_fields(self):
        return super(ResPartner, self)._commercial_fields() + ['team_id']


class PartnerAdapter(Component):
    _name = 'ayurdevas.partner.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.res.partner'

    def read(self, external_field, attributes=None):
        """ Returns the information of a record

        :rtype: dict
        """
        with self.handle_404():
            record = self._call('GetCustomer', external_field)
            return record

    def create(self, record):
        """ Create a record on the external system """
        return self._call('CreateCustomer', record)['id']

    def write(self, external_id, data):
        """ Update records on the external system """
        self._call('UpdateCustomer', external_id, data)


class AyurdevasAddress(models.Model):
    _name = 'ayurdevas.address'
    _inherit = 'ayurdevas.binding'
    _inherits = {'res.partner': 'odoo_id'}
    _description = 'Ayurdevas Ecommerce Address'

    _rec_name = 'backend_id'

    odoo_id = fields.Many2one(
        comodel_name='res.partner',
        string='Partner',
        required=True,
        ondelete='cascade'
    )

    ayurdevas_partner_id = fields.Many2one(
        comodel_name='ayurdevas.res.partner',
        string='Ayurdevas Partner',
        required=True,
        ondelete='cascade'
    )

    backend_id = fields.Many2one(
        related='ayurdevas_partner_id.backend_id',
        comodel_name='ayurdevas.backend',
        string='Ayurdevas Backend',
        store=True,
        readonly=True,
        # override 'ayurdevas.binding', can't be INSERTed if True:
        required=False,
    )

    is_ecommerce_order_address = fields.Boolean(
        string='Address from Ecommerce Order',
    )

    _sql_constraints = [
        ('odoo_uniq', 'unique(backend_id, odoo_id)',
         'A partner address can only have one binding by backend.'),
    ]
