# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.component_event import skip_if


def should_skip(record):
    """ True if the partner has its access disabled from ecommerce
    True if not a root partner

    To be used with :func:`odoo.addons.component_event.skip_if`
    on Events::

        from odoo.addons.component.core import Component
        from odoo.addons.component_event import skip_if


        class MyEventListener(Component):
            _name = 'my.event.listener'
            _inherit = 'base.connector.event.listener'
            _apply_on = ['magento.res.partner']

            @skip_if(lambda: self, record, *args, **kwargs:
                     should_skip(record))
            def on_record_write(self, record, fields=None):
                record.with_delay().export_record()

    """
    if record.parent_id:
        return True
    return not(record.active and record.ayurdevas_ecommerce_access)


class AyurdevasPartnerExportListener(Component):
    _name = 'ayurdevas.partner.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['res.partner']

    @skip_if(lambda self, record, *args, **kwargs: should_skip(record))
    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_write(self, record, fields=None):
        for binding in record.ayurdevas_bind_ids:
            binding.with_delay(
                priority=20,
                description=_('Update partner [%s] %s') % (record.ref, record.name)
            ).export_record()

    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_create(self, record, fields=None):
        """
        Create a ``ayurdevas.res.partner`` record. This record will then
        be exported to Ayurdevas Ecommerce.
        """
        for backend_id in self.env['ayurdevas.backend'].search([]):
            self.env['ayurdevas.res.partner'].create({
                'backend_id': backend_id.id,
                'odoo_id': record.id
            })


class AyurdevasBindingPartnerExportListener(Component):
    _name = 'ayurdevas.binding.partner.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['ayurdevas.res.partner']

    @skip_if(lambda self, record, *args, **kwargs: should_skip(record))
    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_create(self, record, fields=None):
        record.with_delay(
            description=_('Create partner [%s] %s') % (record.ref, record.name)
        ).export_record()

    @skip_if(lambda self, record, *args, **kwargs: should_skip(record))
    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    def on_record_write(self, record, fields=None):
        record.with_delay(
            priority=20,
            description=_('Update partner [%s] %s') % (record.ref, record.name)
        ).export_record()
