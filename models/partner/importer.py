# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import logging

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.connector.components.mapper import mapping, none, only_create
from odoo.addons.connector.exception import InvalidDataError
from odoo.addons.queue_job.exception import RetryableJobError
from ...components.mapper import (
    normalize_date,
    normalize_datetime,
    normalize_name,
    titlecase
)

_logger = logging.getLogger(__name__)


class PartnerImportMapper(Component):
    """
      data:{
        "id": 15378,
        "username": "634180",
        "email": "foo@bar.com",
        "confirmed": 1,
        "category": "VTADTA",
        "vat": "34002225",
        "fiscal_position": 5,
        "document_type": 96,
        "presea_franquicia": 0,
        "presea_iva": 0,
        "name": "CASERES TRINIDAD",
        "presea_tipo": 3,
        "level_1_partner_id": "",
        "level_2_partner_id": 11542,
        "level_3_partner_id": 13672,
        "updated_at": "2020-07-24 14:27:33",
        "created_at": "2020-07-17 19:43:11",
        "type": "venta_directa",
        "start_date": "2019-11-12",
        "stop_date": "2020-01-01",
        "restart_date": "2020-05-03",
      }
      """
    _name = 'ayurdevas.res.partner.mapper'
    _inherit = 'ayurdevas.import.mapper'
    _apply_on = 'ayurdevas.res.partner'

    direct = [
        ('id', 'external_id'),
        ('username', 'ref'),
        (none('email'), 'email'),
        (titlecase(normalize_name('name')), 'name'),
        (normalize_datetime('created_at'), 'created_at'),
        (normalize_datetime('updated_at'), 'updated_at'),
        (normalize_date('start_date'), 'date_start'),
        (normalize_date('end_date'), 'date_stop'),
        (normalize_date('restart_date'), 'date_reincorporation'),
        (none(normalize_name('mobile')), 'mobile')
    ]

    @mapping
    def is_active(self, record):
        """Check if the partner is active in Ecommerce
        and set active flag
        confirmed == 1 in ecommerce means active"""
        return {'active': (record.get('confirmed') == 1)}

    @only_create
    @mapping
    def customer(self, record):
        return {'customer': True, 'supplier': False}

    @only_create
    @mapping
    def type(self, record):
        return {'type': 'contact'}

    @only_create
    @mapping
    def company_id(self, record):
        company = self.backend_record.company_id
        if company:
            return {'company_id': company.id}
        return {'company_id': False}

    @only_create
    @mapping
    def vat_nr(self, record):
        invalids = [
            '0',
            '10',
            '100',
            '1000',
            '10000',
            '100000',
            '1000000',
            '10000000',
            '100000000',
            '1000000000',
            '100000001',
            '1000000001',
        ]

        nif = record['vat']
        if nif not in invalids:
            return {'vat': nif}

    @mapping
    def ecommerce_access(self, record):
        return {'ayurdevas_ecommerce_access': record.get('confirmed') == 1}

    @mapping
    def sponsor_id(self, record):
        sponsor_id = record.get('level_3_partner_id')
        # Skip if sponsor is empty or relation is recursive
        if not sponsor_id or sponsor_id == record.get('id'):
            return

        binder = self.binder_for(model='ayurdevas.res.partner')
        partner = binder.to_internal(sponsor_id, unwrap=True)

        if not partner:
            binding = self.env['ayurdevas.res.partner']
            delayable = binding.with_delay(priority=1, max_retries=3)
            delayable.import_record(self.backend_record, sponsor_id)

            raise RetryableJobError(
                'Missing dependency Sponsor %s for Partner %s, delay import'
                ' until dependency is met' % (sponsor_id, record.get('id'))
            )

        return {'sponsor_id': partner.id}

    @mapping
    def backend_id(self, record):
        return {'backend_id': self.backend_record.id}

    @mapping
    def account_fiscal_position(self, record):
        ifield = str(record.get('fiscal_position') or '')
        if not ifield:
            return

        model = self.env['codes.models.relation']
        try:
            fiscal_position = model.get_record_from_code(
                'account.fiscal.position',
                ifield
            )
        except Warning:
            raise AssertionError("Missing fiscal position with backend code"
                                 " %s. Please create or configure the record"
                                 " first." % ifield
                                 )

        return {'property_account_position_id': fiscal_position.id}

    @mapping
    def partner_document_type(self, record):
        ifield = str(record.get('document_type') or '')
        if not ifield:
            return

        model = self.env['codes.models.relation']
        try:
            document_type = model.get_record_from_code(
                'partner.document.type',
                ifield
            )
        except Warning:
            raise AssertionError("Missing document type with backend code"
                                 " %s. Please create or configure the record"
                                 " first." % ifield
                                 )

        return {'partner_document_type_id': document_type.id}

    @mapping
    def leader_id(self, record):
        order_type = str(record.get('type', '')).lower()
        if order_type != 'venta_directa':
            return

        level_1 = int(record.get('level_1_partner_id') or 0)
        level_2 = int(record.get('level_2_partner_id') or 0)
        leader_id = level_2 or level_1
        # Skip if leader is empty or relation is recursive
        if not leader_id or leader_id == record.get('id'):
            return

        binder = self.binder_for(model='ayurdevas.res.partner')
        partner = binder.to_internal(leader_id, unwrap=True)

        if not partner:
            binding = self.env['ayurdevas.res.partner']
            delayable = binding.with_delay(priority=1, max_retries=3)
            delayable.import_record(self.backend_record, leader_id)

            raise RetryableJobError(
                'Missing dependency Leader %s for Partner %s, delay import'
                ' until dependency is met' % (leader_id, record.get('id'))
            )

        return {'leader_id': partner.id}

    @only_create
    @mapping
    def commercial_fields(self, record):
        """Maps Ecommerce customer type to ResPartner"""
        type_desc = record.get('type')
        config = self.backend_record.find_partner_configuration(type_desc)
        if not config:
            raise AssertionError(
                'Missing Partner configuration for type "%s". '
                'Please create or configure the record' % type_desc
            )

        return {
            'property_payment_term_id': config.payment_term_id.id,
            'property_product_pricelist': config.pricelist_id.id,
            'sale_type': config.sale_type_id.id,
            'team_id': config.team_id.id,
        }

    @mapping
    def state(self, record):
        if not record.get('state'):
            return

        domain = [
            ('name', '=ilike', record['state']),
            ('country_id.name', '=ilike', 'argentina')
        ]
        state = self.env['res.country.state'].search(domain, limit=1)
        if state:
            return {'state_id': state.id, 'country_id': state.country_id.id}


class PartnerImporter(Component):
    _name = 'ayurdevas.res.partner.importer'
    _inherit = 'ayurdevas.base.importer'
    _apply_on = 'ayurdevas.res.partner'

    def _must_skip(self, force=False):
        skip = super(PartnerImporter, self)._must_skip(force=force)
        if skip:
            return skip
        if self.binder.to_internal(self.external_id):
            return _('Already imported')

    def _validate_data(self, data):
        """ Check if the values to import are correct

        Pro-actively check before the ``_create`` or
        ``_update`` if some fields are missing or invalid.

        Raise `InvalidDataError`
        """
        required_fields = {
            'name': 'Name',
            'vat': 'VAT Number',
            'partner_document_type_id': 'Document Type',
            'property_account_position_id': 'Account Position'
        }
        errors = []
        for field, name in required_fields.items():
            if not data.get(field):
                errors.append("%s required" % name)
        if errors:
            raise InvalidDataError(', '.join(errors))
        return

    def _create(self, data):
        binding = super(PartnerImporter, self)._create(data)
        self.backend_record.add_checkpoint(binding)
        return binding


class AddressImportMapper(Component):
    _name = 'ayurdevas.address.import.mapper'
    _inherit = 'ayurdevas.import.mapper'
    _apply_on = 'ayurdevas.address'

    direct = [
        ('cp', 'zip'),
        (none('telefono'), 'mobile'),
        (none('mail'), 'email'),
        (titlecase(normalize_name('ciudad')), 'city'),
    ]

    @mapping
    def state(self, record):
        domain = [
            ('name', '=ilike', record['provincia']),
            ('country_id.name', '=ilike', 'argentina')
        ]
        state = self.env['res.country.state'].search(domain, limit=1)
        if state:
            return {'state_id': state.id, 'country_id': state.country_id.id}

    @only_create
    @mapping
    def type(self, record):
        return {'type': 'delivery'}

    @mapping
    def street(self, record):
        value = record['calle'].strip()
        if not value:
            return

        address = ['{street} {nr}'.format(
            street=value.strip().title(),
            nr=record['numero'].strip().upper(),
        )]

        floor_nr = record['piso'].strip().upper()
        if floor_nr:
            address += ['Piso {floor}'.format(floor=floor_nr)]

        depto = record['depto'].strip().upper()
        if depto:
            address += ['Dpto. {nr}'.format(nr=depto)]

        return {'street': ', '.join(address)}

    @only_create
    @mapping
    def company_id(self, record):
        parent = self.options.parent_partner
        if parent:
            if parent.company_id:
                return {'company_id': parent.company_id.id}
            else:
                return {'company_id': False}
        else:
            company = self.backend_record.company_id
            if company:
                return {'company_id': company.id}

    @only_create
    @mapping
    def name(self, record):
        value = record['calle'].strip()
        if not value:
            return

        address = '{street} {nr}'.format(
            street=value.strip().title(),
            nr=record['numero'].strip().upper(),
        )
        return {'name': address}
