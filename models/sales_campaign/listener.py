# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component
from odoo.addons.component_event import skip_if


class AyurdevasSalesCampaignListener(Component):
    _name = 'ayurdevas.sales.campaign.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['sales.campaign']

    def on_record_write(self, record, fields=None):
        for binding in record.ayurdevas_bind_ids:
            if not binding.backend_id.export_sales_campaign:
                continue

            binding.with_delay(
                description=_('Updating campaign %s') % (record.name, )
            ).export_record()

    def on_record_create(self, record, fields=None):
        """
        Create a ``ayurdevas.sales.campaign`` record. The record will then
        be exported to Ayurdevas Ecommerce.
        """
        for backend_id in self.env['ayurdevas.backend'].search([]):
            if backend_id.export_sales_campaign:
                self.env['ayurdevas.sales.campaign'].create({
                    'backend_id': backend_id.id,
                    'odoo_id': record.id
                })


class AyurdevasBindingSalesCampaignListener(Component):
    _name = 'ayurdevas.binding.sales.campaign.export.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['ayurdevas.sales.campaign']

    @skip_if(lambda self, record, *args, **kwargs: self.no_connector_export(record))
    @skip_if(lambda self, record, *args, **kwargs: not record.backend_id.export_sales_campaign)
    def on_record_create(self, record, fields=None):
        record.with_delay(
            description=_('Creating campaign %s') % (record.name, )
        ).export_record()
