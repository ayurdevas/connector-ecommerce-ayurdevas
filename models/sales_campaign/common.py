# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import fields, models
from odoo.addons.component.core import Component


class AyurdevasSalesCampaign(models.Model):
    _name = 'ayurdevas.sales.campaign'
    _inherit = 'ayurdevas.binding'
    _inherits = {'sales.campaign': 'odoo_id'}
    _description = 'Ayurdevas Sales Campaign'

    odoo_id = fields.Many2one(
        comodel_name='sales.campaign',
        string='Product',
        required=True,
        ondelete='cascade'
    )


class SalesCampaign(models.Model):
    _inherit = 'sales.campaign'

    ayurdevas_bind_ids = fields.One2many(
        comodel_name='ayurdevas.sales.campaign',
        inverse_name='odoo_id',
        string='Ayurdevas Bindings',
        copy=False,
    )


class SalesCampaignAdapter(Component):
    _name = 'ayurdevas.sales.campaign.adapter'
    _inherit = 'ayurdevas.adapter'
    _apply_on = 'ayurdevas.sales.campaign'

    def read(self, external_field, attributes=None):
        """ Returns the information of a record """
        with self.handle_404():
            record = self._call('GetCampaign', external_field)
            return record

    def create(self, record):
        """ Create a record on the external system """
        return self._call('CreateCampaign', record)['id']

    def write(self, external_id, data):
        """ Update records on the external system """
        self._call('UpdateCampaign', external_id, data)
