# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo.tools import date_utils
from odoo.addons.component.core import Component
from odoo.addons.connector.exception import InvalidDataError
from odoo.addons.connector.components.mapper import mapping


class SalesCampaignExporter(Component):
    """ Export Sales Campaign to Ayurdevas Ecommerce """
    _name = 'ayurdevas.sales.campaign.exporter'
    _inherit = 'ayurdevas.base.exporter'
    _apply_on = 'ayurdevas.sales.campaign'

    def _should_import(self):
        return False


class SalesCampaignExportMapper(Component):
    _name = 'ayurdevas.sales.campaign.export.mapper'
    _inherit = 'ayurdevas.export.mapper'
    _apply_on = ['ayurdevas.sales.campaign']

    direct = [
        ('name', 'name'),
    ]

    @mapping
    def sales_team(self, record):
        team_id = record.team_id

        if not team_id:
            return

        # Requiere sudo porque ayurdevas_code pertenece al grupo de permisos
        # connector.group_connector_manager
        team_id = team_id.sudo()
        if not team_id.ayurdevas_code:
            raise AssertionError("Missing backend code for Sales Team"
                                 " '%s'. Please create or configure the code"
                                 " first." % team_id.name
                                 )

        return {'sales_team': team_id.ayurdevas_code}

    @mapping
    def date_start(self, record):
        return {'date_start': date_utils.json_default(record.date_start)}

    @mapping
    def date_stop(self, record):
        return {'date_end': date_utils.json_default(record.date_stop)}
