# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

import logging

from contextlib import contextmanager
from datetime import datetime, timedelta

from odoo import api, fields, models, _
from odoo.addons.connector.models import checkpoint
from odoo.addons.queue_job.job import job
from ...components.backend_adapter import AyurdevasAPI

_logger = logging.getLogger(__name__)

IMPORT_DELTA_BUFFER = 30  # seconds


class AyurdevasBackend(models.Model):
    _name = 'ayurdevas.backend'
    _description = 'Ayurdevas Ecommerce Backend'
    _inherit = 'connector.backend'

    REGISTER_PAYMENT_METHODS = [
        'tarjeta',
        'tarjeta_debito',
        'tarjeta_credito',
        'pago_facil',
        'rapipago',
        'bapro',
        'saldo_mercadopago',
    ]

    @api.model
    def _get_stock_field_id(self):
        domain = [
            ('model', '=', 'product.product'),
            ('name', '=', 'virtual_available')
        ]
        return self.env['ir.model.fields'].search(domain, limit=1)

    @api.model
    def _default_warehouse_id(self):
        company = self.env.user.company_id.id
        warehouse_ids = self.env['stock.warehouse'].search(
            [('company_id', '=', company)],
            limit=1
        )
        return warehouse_ids

    name = fields.Char()

    location = fields.Char(
        string='Location',
        required=True,
        help='Url to Ayurdevas ecommerce application',
    )

    access_token = fields.Char(
        string='Access Token',
        help='Token for web server authentication',
    )

    import_orders_from_date = fields.Datetime(
        string='Import sale orders from date',
        help='do not consider non-imported sale orders before this date. '
             'Leave empty to import all sale orders',
    )

    no_sales_order_sync = fields.Boolean(
        string='No Sale Orders Synchronization',
        help='Check if sale orders should not be imported',
    )

    no_sales_order_state_export = fields.Boolean(
        string='No Sale Orders State Export',
        help='Check if sale orders state should not be exported',
    )

    sale_prefix = fields.Char(
        string='Sale Prefix',
        help="A prefix put before the name of imported sales orders.\n"
             "For instance, if the prefix is 'mag-', the sales "
             "order 100000692 in Ecommerce, will be named 'mag-100000692' "
             "in Odoo.",
    )

    partner_configuration = fields.One2many(
        'ayurdevas.backend.partner.configuration',
        'backend_id',
        string='Partner Configuration',
        copy=True,
        auto_join=True
    )

    import_sale_order_payments = fields.Boolean(
        string='Import Payments',
    )

    payment_method_id = fields.Many2one(
        'account.payment.method',
        string='Payment Method Type',
    )

    journal_id = fields.Many2one(
        'account.journal',
        string='Payment Journal',
        domain=[('type', 'in', ('bank', 'cash'))],
    )

    account_payment_type_id = fields.Many2one(
        'account.payment.type',
        'Linea de metodo de pago',
    )

    pos_ar_id = fields.Many2one(
        'pos.ar',
        string='Punto de venta',
    )

    export_sales_campaign = fields.Boolean(
        string='Export Campaigns',
        help='Check if campaigns should be exported'
    )

    export_points_line = fields.Boolean(
        string='Export Points',
        help='Check if points line should be exported'
    )

    export_miles_line = fields.Boolean(
        string='Export Miles',
        help='Check if miles line should be exported'
    )

    export_stock_quants = fields.Boolean(
        string='Export Stock Quantities',
        help='Check if quantities should be exported',
    )

    product_stock_field_id = fields.Many2one(
        comodel_name='ir.model.fields',
        string='Stock Field',
        default=_get_stock_field_id,
        domain="[('model', 'in', ['product.product', 'product.template']),"
               " ('ttype', '=', 'float')]",
        help="Choose the field of the product which will be used for "
             "stock inventory updates.\nIf empty, Quantity Available "
             "is used.",
    )

    warehouse_id = fields.Many2one(
        comodel_name='stock.warehouse',
        string='Warehouse',
        required=True,
        default=_default_warehouse_id,
        help="Warehouse used to compute the stock quantities.\n"
             "In a context where no Stock Location is given, this includes "
             "goods stored in the Stock Location of this Warehouse, or any of "
             "its children."
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        related='warehouse_id.company_id',
        string='Company',
        readonly=True,
    )

    location_ids = fields.Many2many(
        comodel_name='stock.location',
        domain="[('company_id', '=', company_id)]",
        string='Locations',
        help="Locations used to compute the stock quantities.\n"
             "This includes goods stored at this locations, or any of its "
             "children.\n"
             "If no location is given, default the Stock Location of the "
             "Warehouse will be used"
    )

    export_account_balance = fields.Boolean(
        string='Export Account Balance',
        help='Check if balance should be exported',
    )

    account_balance_team_ids = fields.Many2many(
        comodel_name='crm.team',
        domain="[('company_id', '=', company_id)]",
        string='Sales Team',
        help="'Only Partners in this Sales Team will have their account balance'\n"
             "computed for export.\n "
             "If none is given, all account balances will be computed"
    )

    @contextmanager
    @api.multi
    def work_on(self, model_name, **kwargs):
        self.ensure_one()
        # We create a Ayurdevas Client API here, so we can create the
        # client once and propagate it through all the sync session,
        # instead of recreating a client in each backend adapter usage.
        with AyurdevasAPI(self.location, self.access_token) as ayurdevas_api:
            _super = super(AyurdevasBackend, self)
            # from the components we'll be able to do: self.work.ayurdevas_api
            with _super.work_on(
                    model_name, ayurdevas_api=ayurdevas_api, **kwargs) as work:
                yield work

    @api.multi
    def import_sale_orders(self):
        """ Import sale orders """
        import_start_time = datetime.now()
        # for storeview in self:
        for backend in self:
            # if storeview.no_sales_order_sync:
            if backend.no_sales_order_sync:
                _logger.debug("The backend '%s' is active "
                              "but is configured not to import the "
                              "sales orders", backend.name)
                continue

            if self.import_orders_from_date:
                from_string = fields.Datetime.from_string
                from_date = from_string(self.import_orders_from_date)
            else:
                from_date = None

            sale_binding_model = self.env['ayurdevas.sale.order']
            # user = storeview.sudo().warehouse_id.company_id.user_tech_id
            # if not user:
            #     user = self.env['res.users'].browse(self.env.uid)
            # if user != self.env.user:
            #     sale_binding_model = sale_binding_model.sudo(user)

            delayable = sale_binding_model.with_delay(
                priority=1,
                max_retries=5,
                description=_("Prepare the import of Sales Orders"),
            )
            filters = {
                'from_date': from_date,
                'to_date': import_start_time,
            }
            delayable.import_batch(backend, filters=filters)
        # Records from Ayurdevas Ecommerce are imported based on their
        # `created_at` date. This date is set on Ayurdevas Ecommerce at the
        # beginning of a transaction, so if the import is run between the
        # beginning and the end of a transaction, the import of a record may be
        # missed.  That's why we add a small buffer back in time where
        # the eventually missed records will be retrieved.  This also
        # means that we'll have jobs that import twice the same records,
        # but this is not a big deal because the sales orders will be
        # imported the first time and the jobs will be skipped on the
        # subsequent imports
        next_time = import_start_time - timedelta(seconds=IMPORT_DELTA_BUFFER)
        next_time = fields.Datetime.to_string(next_time)
        self.write({'import_orders_from_date': next_time})
        return True

    @api.multi
    def import_res_partners(self):
        pass

    def find_partner_configuration(self, type_string):
        return self.partner_configuration.search(
            [('partner_type_string', '=', type_string)]
        )

    @api.multi
    def add_checkpoint(self, record):
        self.ensure_one()
        record.ensure_one()
        return checkpoint.add_checkpoint(self.env, record._name, record.id,
                                         self._name, self.id)

    @api.model
    def should_import_payment(self, payment_method):
        return (self.import_sale_order_payments and
                payment_method in AyurdevasBackend.REGISTER_PAYMENT_METHODS)

    @api.multi
    def update_product_stock_qty(self):
        product_product = self.env['ayurdevas.product.product']
        domain = self._domain_for_update_product_stock_qty()
        ayurdevas_products = product_product.search(domain)
        ayurdevas_products.recompute_ecommerce_qty()
        return True

    def action_update_product_stock_qty(self):
        self.update_product_stock_qty()

    @api.multi
    def _domain_for_update_product_stock_qty(self):
        domain = [
            ('backend_id', 'in', self.filtered(lambda r: r.export_stock_quants).ids),
            ('type', '!=', 'service'),
            ('no_stock_sync', '=', False),
        ]
        if self.env.context.get('update_product_stock', False):
            domain += [('odoo_id', 'in', self.env.context['update_product_stock'])]
        return domain

    @api.model
    def _ayurdevas_backend(self, callback, domain=None):
        if domain is None:
            domain = []
        backends = self.search(domain)
        if backends:
            getattr(backends, callback)()

    @api.model
    def _scheduler_import_sale_orders(self, domain=None):
        self._ayurdevas_backend('import_sale_orders', domain=domain)

    @api.model
    def _scheduler_import_res_partners(self, domain=None):
        self._ayurdevas_backend('import_res_partners', domain=domain)

    @api.model
    def _scheduler_update_product_stock_qty(self, domain=None):
        self._ayurdevas_backend('update_product_stock_qty', domain=domain)

    @api.model
    def _scheduler_update_partners_total_due(self, domain=None):
        self._ayurdevas_backend('update_partners_total_due', domain=domain)

    @job(default_channel='root.ayurdevas')
    @api.multi
    def update_picking_stock_qty(self, stock_picking_id):
        picking_id = self.env['stock.picking'].browse(stock_picking_id)
        product_ids = picking_id.mapped('move_lines.product_id').ids
        ctx = self.with_context(update_product_stock=product_ids)
        ctx.update_product_stock_qty()

    @api.multi
    def update_partners_total_due(self):
        model = self.env['ayurdevas.res.partner']
        domain = self._domain_for_update_partners_total_due()
        partner_ids = model.search(domain)
        partner_ids.recompute_ecommerce_total_due()

    @api.multi
    def _domain_for_update_partners_total_due(self):
        backend_ids = self.filtered(lambda r: r.export_account_balance)
        domain = [
            ('backend_id', 'in', backend_ids.ids),
            ('external_id', 'not like', 'guestorder:'),
        ]
        team_ids = self.account_balance_team_ids.ids
        if team_ids:
            domain += [('team_id', 'in', team_ids)]
        return domain


class AyurdevasBackendPartnerConfiguration(models.Model):
    _name = 'ayurdevas.backend.partner.configuration'
    _description = 'Ayurdevas Ecommerce Backend Partner configuration line'

    backend_id = fields.Many2one(
        'ayurdevas.backend',
        string='Backend Reference',
        required=True,
        ondelete='cascade',
        index=True,
        copy=False
    )

    partner_type_string = fields.Char(string='Partner Type', required=True)

    payment_term_id = fields.Many2one(
        'account.payment.term',
        string='Payment Term',
        required=True
    )

    pricelist_id = fields.Many2one(
        'product.pricelist',
        string='Pricelist',
        required=True
    )

    sale_type_id = fields.Many2one(
        'sale.order.type',
        string='',
        required=True)

    team_id = fields.Many2one('crm.team', string='Sales Team', required=True)
