# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import _
from odoo.addons.component.core import Component


class AyurdevaStockPickingListener(Component):
    _name = 'ayurdevas.stock.picking.listener'
    _inherit = 'base.connector.listener'
    _apply_on = ['stock.picking']

    def on_picking_dropship_done(self, record, picking_method):
        return self.on_picking_out_done(record, picking_method)

    def on_picking_out_done(self, record, picking_method):
        """
        Export SaleOrder status change: pending -> processed

        :param picking_method: picking_method, can be 'complete' or 'partial'
        :type picking_method: str
        """
        self._update_backend_stock(record)
        self._sale_order_picking_done(record)

    def _sale_order_picking_done(self, record):
        sale = record.sale_id
        if not sale:
            return
        for binding in sale.ayurdevas_bind_ids:
            binding.with_delay(
                description=_('Picking done for sales order %s') % (sale.name, )
            ).export_state_change(state='picking_done')

    def _update_backend_stock(self, record):
        """
        Updates stock quantity for products in picking
        """
        warehouse_id = record.mapped('picking_type_id.warehouse_id')
        domain = [('warehouse_id', '=', warehouse_id.id)]
        backend_ids = self.env['ayurdevas.backend'].search(domain)
        if not backend_ids:
            return

        backend_ids.with_delay(
                priority=0,
                description='Update Stock for Picking %s' % (record.name, )
            ).update_picking_stock_qty(record.id)
